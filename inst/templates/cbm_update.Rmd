---
title: "Run CBM to update NFI to year 2015 for: `r countryiso2`"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  html_document: 
    keep_md: yes
    toc: yes
---

```{r setup, include=FALSE}
# This document is a template to run a CBM script for a particular country
# The datapath and countryiso2 variables are defined outside the scope of this document, 
# in an R command before the rmarkdown::render() function call.
# More information is available in the cbmio package repository, under docs/templates.Rmd.
datapathcountry <- normalizePath(file.path(datapath, countryiso2))
startofmodelrun <- Sys.time()
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_knit$set(root.dir = "C:\\Dev\\cbmwrap") 
library(dplyr)
library(cbmio)
```



## Configuration

The general configuration file contains data and software paths necessary to run CBM scripts:
```{r cbmwrapconfig, comment=""}
message("Content of : ", normalizePath("cbmwrapconfig.yaml"))
cat(readLines("cbmwrapconfig.yaml"), sep="\n")
# Read the config file and create an R list from it
config <- yaml::read_yaml("cbmwrapconfig.yaml")
# Check if the basepath given in the config file is the same as the root.dir given to knit this document
stopifnot(identical(normalizePath(getwd()), normalizePath(config$basepath)))
```

The optional country specific configuration file contains processing option specific to one country:
```{r cbmwrapcountryconfig}
# Find the path to all files starting with cbmwrapcountryconfig in the country data folder
countryconfig <- list.files(datapathcountry, pattern = "cbmwrapcountryconfig*", full.names = TRUE)
tryCatch(readLines(countryconfig), 
         error = function(e) message("\nThere is no configuration file in ", datapathcountry, "."))
```

Check if the necessary files are available to run the model
```{r check_if_BACK_Inventory_AWS_is_present}
# This could be made dynamic, based on which file is actually used by the model
if(!file.exists(file.path(datapathcountry, "BACK_Inventory_AWS.csv"))){
    warning("\n\nError: Missing **BACK_Inventory_AWS.csv** in ", datapathcountry, 
            ".\nRun the upd_aws.py and fusion_aws scripts first.\n\n")
    knit_exit()
}
```


## Run CBM to update NFI to year 2015
The following command line:
```{r run_cbm_update}
endyear <- 2015
pythonscript <- normalizePath(file.path(config$basepath, "cbm_update.py"))
message(sprintf('python "%s" "%s" "%s" %s',
                pythonscript, datapath, countryiso2, endyear))
systemout <- system2("python", 
                     args = c(pythonscript, datapath, countryiso2, endyear), 
                     stdout = TRUE, # Sent output to the R console but do not store standard output in the markdown document.
                     stderr = TRUE)
```


Generated this output:
```{r print_the_output_of_cbm_update, comment=NA}
message("\n") # Line break
# Remove duplicated lines
systemout <- systemout[Map(adist, systemout[-length(systemout)], systemout[-1]) > 6]

# Print the output
cat(removesimilarlines(systemout), sep="\n")
```


Files which were modified in the last hour:
```{r show_which_files_were_modified, echo=FALSE, message=FALSE, warning=FALSE}
filemodiftime <- 3600 # In seconds
message("New files located in: ", datapathcountry)
countryfiles <- list.files(datapathcountry, full.names = TRUE, recursive = TRUE)
countryfiles <- data.frame(pathString = countryfiles,
                           size = file.size(countryfiles),
                           mtime = file.mtime(countryfiles)) %>% 
    filter(mtime > Sys.time() - filemodiftime)

if(require(data.tree) & require(gdata)){ # Print a tree with file sizes and modiftime if these packages are available
    try({
        countryfiles <- countryfiles %>% 
            mutate(sizeh = gdata::humanReadable(size, standard ="Unix", width = 5),
                   mtimeh = format(mtime, "%Y-%m-%d %H:%M")) 
        message("The largest files modified recently under ", datapathcountry, " :")
        countryfiles %>% 
            filter(size > (2^{10})^2) %>% # size > one mebibyte see ?humanReadable
            data.tree::as.Node() %>% 
            print("sizeh", "mtimeh")
    message("All files modified recently under ", datapathcountry, " :")
    countryfiles %>% 
        data.tree::as.Node() %>% 
        print("sizeh", "mtimeh")
    })
}
```


## Inspect model run output

### C:/CBM/data/SI/2015/Makelist_output/1/1.mdb
Stackoverflow answer concerning [Scatter plots and bar charts in ascii mode](https://stackoverflow.com/a/14742722/2641825)

```{r}

# txtbarchart is a text based barplot 
# and plots the relative frequences of the occurences of the different levels of a factor (in percent)
# txtbarchart(as.factor(c(letters[1:5])), pch="I")

```

Length of the run:
```{r}
message("start ", startofmodelrun, " end ", Sys.time())
Sys.time() - startofmodelrun
```
