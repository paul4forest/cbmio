[TOC]

An R package that prepares input data and reads output data from the Carbon Budget Model 


# Installation

The source code us avaukavke on CITnet, under
[BIOECONOMY/repos/cbmio](https://webgate.ec.europa.eu/CITnet/stash/projects/BIOECONOMY/repos/cbmio/browse).

To install the package, replace `CITnet_user_name` below by your CITnet user name,
then run the following at an R command prompt: 

    install.packages(c("dplyr", "purrr", "tidyr")) # cbmio required packages
    install.packages(c("knitr", "rmarkdown")) # cbmio suggested packages
    install.packages(c("getPass", "devtools")) # Tools only needed to install from CITNET
    devtools::install_git("https://rougipa@webgate.ec.europa.eu/CITnet/stash/scm/bioeconomy/cbmio.git", credentials = git2r::cred_user_pass("CITnet_user_name", getPass::getPass()))

# Usage

Load the package

    library(cbmio)

Load inventory data from CBM for Italy and Germany in one data frame:

    inventoryitde <- readcbm("BACK_inventory", "C:/CBM/data", c("IT","DE"))
    
Load the nested version of the same inventory data

    inventoryitde <- readcbmnested("BACK_inventory", "C:/CBM/data", c("IT","DE"))

Perform diagnostics in case column names are different in the different countries

    checkcolumns(inventoryitde)


# Building principles 

Concepts to keep in mind while writing code for cbmio:

 * create **plumbing functions** that can be re-used in various programs
     * write tests first before writting plumbing functions
 * create **porcelaine functions** to facilitate scripting around the Carbon Budget Model and its wraper scripts

It's fine to write porcelaine functions, but whenever possible
most of the computation they perform should be moved to plumbing functions.


## Fonction interface

Function documentation provides a description of the interface.
See for example the documentation of the aligntransitiontodisturbance function by 
pressing `?aligntransitiontodisturbance` at a command prompt. 
