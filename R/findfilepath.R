#' Find a file available in the given country folder that matches the given pattern
#'
#' @description \code{findfilepath} will look for a file name pattern associated with a CBM access database name or
#' with a csv file returned by FUSION.
#' There whould normally be only one file matching the given pattern.
#' The function will give a warning if there are zero or more than one file.
#' @param datapathcountry character path to a country folder
#' @param subfolder character name of the subfolder in which calibration data is saved
#' @param pattern regular expression, only file names which match the regular expression will be returned.
#' @param warning logical if TRUE, a warning is returned when there is no file or more than one file
#' @param error logical if TRUE, an error is returned when there is no file or more than one file
#' @return a character vector containing a file path
#' @examples
#' \donttest{
#' # Find the path to the CBM calibration database for Belgium
#' findfilepath("C:/CBM/data/BE", "from_CBM_calibration", ".*Const.*mdb$")
#' # Find the path to the BACK_inventory_AWS.csv file for Belgium
#' findfilepath("C:/CBM/data/BE", "", "_AWS.csv")
#'
#' # Warnings and errors
#' # Generate a warning for a non exisiting file
#' findfilepath("","","non existing")
#' # Generate an error instead
#' findfilepath("","","non existing", error = TRUE)
#' # Fail silently, without warning
#' findfilepath("","","non existing", warning = FALSE)
#' }
#' @export
findfilepath <- function(datapathcountry,
                         subfolder,
                         pattern,
                         warning = TRUE,
                         error = FALSE){
    filepath <- list.files(file.path(datapathcountry, subfolder),
                           pattern = pattern,
                           full.names = TRUE)
    # Give an error or a warning if there is no file or more than one file
    if(!identical(length(filepath), 1L)){
        issue <- gettext("\nThere is an issue in ", datapathcountry, ".",
                         "\nNumber of access files matching the pattern '", pattern, "': " ,
                         length(filepath), "\n",
                         paste(filepath, collapse = "\n"), "\n\n")
        if(error){
            stop(issue)
        } else if(warning){
            warning(issue)
        }
    }
    # If filepath is empty, return NA_character_.
    # NA_character_ enables easier flattening to a character vector in purrr nested data frames.
    if(identical(filepath, character(0))){
        return(NA_character_)
    }
    return(filepath)
}


#' @rdname findfilepath
#' @param datapathcountry character path to a country folder
#' @param colname character column name to give inside the data frame returned by \code{findfilepath4all}
#' @param ... Used to absorb unused components of input list .l in \code{\link{pmap}}
#' @description \code{findfilepath4all} finds the path to all files that match the given pattern.
#' It returns a data frame which also contains the file modification date as an additional column.
#' Inspired by https://stackoverflow.com/questions/45169986/use-dplyr-mutate-in-programming
#' And https://cran.r-project.org/web/packages/dplyr/vignettes/programming.html
#' @examples
#' \donttest{
#' datapath <- "C:/CBM/data/"
#'
#' # Find the path to the calibration database for all countries
#' cbmcalibrationdb <- findfilepath4all(datapath, subfolder = "from_CBM_calibration",
#'                                      pattern = ".*Const.*mdb$", colname = "calibdb")
#' cbmcalibrationdb
#'
#' # Find the path to the calibration database and the sas file in all countries
#' calibrationfiles <- data_frame(datapath = datapath,
#'                                subfolder = rep("from_CBM_calibration", 2),
#'                                pattern = c(".*Const.*mdb$", ".sas"))
#' calibrationfiles <- calibrationfiles %>%
#'     mutate(data = pmap(calibrationfiles, findfilepath4all)) %>%
#'     unnest()
#'
#' # Find the path to all "cbm_project.mdb" databases in the "2015/Makelist_output" folder
#' cbmstatus2015 <- findfilepath4all(datapath, subfolder = "2015/Makelist_output",
#'                                   pattern = "cbm_project.mdb", colname = "mklistoutput2015")
#' cbmstatus2015
#'
#' # check for the presence of BACK_Inventory_AWS.csv and inventory2015.csv in all countries
#' backinventoryaws <- findfilepath4all(datapath, subfolder = "", pattern = "BACK_Inventory_AWS.csv", colname = "backinventory")
#' inventory2015 <- findfilepath4all(datapath, subfolder = "", pattern = "inventory2015.csv", colname = "inventory2015")
#' inventoryfiles <- backinventoryaws %>%
#'     left_join(inventory2015, by = "folder") %>%
#'     select(folder, contains("mtime"))
#' }
#' @export
findfilepath4all <- function(datapath, subfolder, pattern, colname = "file", warning = TRUE, error = FALSE, ...){
    require(dplyr)
    colnamemtime <- paste0(colname, "mtime")
    dtf <- data_frame(folder = list.files(datapath)) %>%
        filter(nchar(folder) == 2) %>%
        mutate(!!colname := purrr::map_chr(file.path(datapath, folder),
                                           findfilepath,
                                           subfolder = subfolder,
                                           pattern = pattern,
                                           warning = warning,
                                           error = error),
               !!colnamemtime := file.mtime(!!as.name(colname)))
}




#' @rdname findfilepath
#' @description \code{findaccessfilepath} specifically find the path of access database files in the calibration database
#' If no country codes are given ( the default), it returns all database paths available in the datapath directory.
#' If country codes aregiven, it searches only inside those country's repositories.
#' @examples
#' \dontrun{
#' # Find database file paths for specific countries
#' findaccessfilepath(datapath, c("IT","DE"))
#' # Find database file paths for all countries
#' findaccessfilepath(datapath)
#' }
#' @return a data frame of country codes and corresponding path to database files.
#' @export
findaccessfilepath <- function(datapath,
                               countryiso2 = NULL,
                               subfolder = "from_CBM_calibration",
                               pattern = ".*Const.*mdb$"){
    require(dplyr)
    require(tidyr)
    require(purrr)
    if (is.null(countryiso2)){
        # If countryiso2 is not given (the default):
        # Discover the name of all 2 digit folders in data path
        # Get the list of coutnry iso 2 codes in data path.
        countryfolders <- data_frame(path = list.files(datapath, full.names = TRUE)) %>%
            mutate(countryiso2 = basename(path)) %>%
            filter(nchar(countryiso2) ==2) # keep only folder names containing a 2 letter country code
    } else {
        # If a vector of countryiso2 codes is given:
        # Create a dataframe with their folder path.
        # Check that the paths exist for each country code.
        countryfolders <- data_frame(path = file.path(datapath, countryiso2),
                                     countryiso2 = countryiso2)
        stopifnot(file.exists(countryfolders$path))
    }

    # Find the access file names based on the given pattern
    accessfiles <- countryfolders %>%
        mutate(accessfilepath = map(path, findfilepath,
                                    subfolder = subfolder, pattern = pattern)) %>%
        group_by(countryiso2, path) %>%
        mutate(nfile = as_vector(map(accessfilepath, length))) %>%
        unnest()
    # Check the number of files for each country (should be one only)
    if(any(as_vector(accessfiles$nfile)>1)){
        warning("These folders have more than one access file:")
        accessfiles %>% filter(nfile>1) %>% unnest() %>%  knitr::kable() %>% print()
    }
    return(accessfiles)
}

