#' Run CBM and FUSION scripts inside markdown templates and render the html and markdown output as a log file
#'
#' @description \code{cbmrender} runs a particular cbm or fusion script,
#' as detailed in the template.
#' Templates are available inside the \code{cbmio} pacakge
#' at \code{system.file("templates", package="cbmio")}.
#' The datapath, countryiso2 and logpath arguments are available for use inside the template.
#' @param template character name of a Rmd template
#' @param countryiso2 character iso 2 code of a country, "all" is a special value for templates valid for all countries
#' @param datapath character path containing CBM data
#' @param logpath character path where the output of this template will be stored
#' @param templatepath character path to the template file
#' @examples
#' \donttest{
#' # To run the CBM and FUSIOn models through templates, start an R session from DOS:
#' # "C:\Program Files\R\R-3.5.0\bin\R.exe"
#' # Then paste the lines below to run models through
#' # the templates stored inside the cbmio package.
#' # Standard output and errors from the models
#' # will be stored in markdown documents, inside the logcbm repository.
#'
#' # Run all modelling steps for all countries
#' # List sub-folders corresponding to country codes and render templates for all countries available in datapath
#' datapath <- "B:\\BIOMASS\\forbiomod1\\EFDM\\CBM\\"
#' logpath <- "C:/Dev/cbmlog"
#' folders <- list.files(datapath)
#' folders <- folders[nchar(folders) == 2]
#' lapply(folders, cbmrender2, datapath = datapath, logpath = logpath)
#'
#' # Run all modeling steps for the given vector of country codes
#' cbmrender2(c("DE", "IE", "PL"), datapath, logpath)
#'
#' # Run all modelling steps for a single, given countryiso2.
#' library(cbmio)
#' countryiso2 <- "CZ"
#' datapath <- "B:\\BIOMASS\\forbiomod1\\EFDM\\CBM\\"
#' logpath <- "C:/Dev/cbmlog"
#' cbmrender("upd_aws.Rmd", countryiso2, datapath, logpath)
#' cbmrender("fusion_aws.Rmd", countryiso2, datapath, logpath)
#' cbmrender("cbm_update.Rmd", countryiso2, datapath, logpath)
#' cbmrender("cbm_mws.Rmd", countryiso2, datapath, logpath)
#' cbmrender("fusion_DT2016_2020.Rmd", countryiso2, datapath, logpath)
#' cbmrender("cbm_run.Rmd", countryiso2, datapath, logpath)
#' cbmrender("index.Rmd", "all", datapath, logpath)
#' }
#' @export
cbmrender <- function(template, countryiso2, datapath, logpath,
                      templatepath = system.file("templates", package="cbmio")){
    if(identical(Sys.getenv("RSTUDIO_PANDOC"),"")){
        Sys.setenv(RSTUDIO_PANDOC='C:/Program Files/RStudio/bin/pandoc')
    }
    if(identical(countryiso2, "all")){
        subfolder <- ""
    } else {
        subfolder <- gsub(".Rmd", "", template)
    }
    rmarkdown::render(input = file.path(templatepath, template),
                      output_file = sprintf('%s/%s/%s.html',
                                            logpath,
                                            subfolder,
                                            countryiso2))
}


#' @rdname cbmrender
#' @description \code{cbmrender2} renders many templates for a given country.
#' Try not to wrap \code{try} statements around this function.
#' The try and except statements should be in the templates,
#' so that we can keep track of error messages in the (markdown) log output files.
#' @param scripts character vector of script names without extension
#' @param index character name of the index script
#' @examples
#' \donttest{
#' }
#' @export
cbmrender2 <- function(countryiso2, datapath, logpath,
                       scripts = c("upd_aws",  "fusion_aws", "cbm_update", "cbm_mws",
                                   "fusion_DT2016_2020", "cbm_run"),
                       index = "index"){
    i <- 1
    for(i in 1:length(scripts)){
        cbmrender(sprintf("%s.Rmd", scripts[i]), countryiso2, datapath, logpath)
        errorstatus <- finderrorstatus(file.path(logpath, scripts[i], sprintf("%s.html", countryiso2)))
        if(!identical("OK", errorstatus)) break
    }
    cbmrender(sprintf("%s.Rmd", index), "all", datapath, logpath)
}


#' Deprecated Render a markdown template that runs a CBM script for a particular country
#' @description \code{runcbmtemplate} is depreceted. use cbmrender instead.
#' This function renders the template multiple times, for all countries available in datapath.
#' The datapath and countryiso2 variables are defined outside the scope of the template.
#' in an R command before the rmarkdown::render() function call.
#' @param datapath character path to CBM data
#' @param template character path to an R markdown template document
#' @param outputpath character path where the output markdown document will be located
#' @param RSTUDIO_PANDOC character path environment variable for the Pandoc path
#' @examples
#' \donttest{
#' # Change the various paths to relevant paths on your system
#' runcbmtemplate(datapath = "C:/CBM/data/" ,
#'                template = 'C:/CBM/cbmwrap/docs/templates/template_upd_aws.Rmd',
#'                outputpath = 'C:/CBM/cbmlog/upd_aws/')
#' }
#' @export
runcbmtemplate <- function(datapath, template, outputpath,
                           RSTUDIO_PANDOC = 'C:/Program Files/RStudio/bin/pandoc'){
    .Deprecated(new = "cbmrender", package = "cbmio",
                msg = "'runcbmtemplate' is deprecated. Use 'cbmrender' instead. see help('cbmrender')")
    message("Make the template files part of the cbmio package, in the /inst folder",
            " so that calling them becomes easier. Beware that the python wraper script location is defined in the cbmwrapconfig.yaml file so that config file probably has to be moved to cbmio too. Or the general path to cbmwrap should be passed to the template as a variable.")
    # Create a vector of country codes
    files <- list.files(datapath)
    files <- files[nchar(files)==2]
    if(!file.exists(RSTUDIO_PANDOC)){
        stop("Pandoc is required for rmarkdown::render",
             "\nIt cannot be found because the directory mentionned in RSTUDIO_PANDOC doesn't exist:\n",
             RSTUDIO_PANDOC,
             "\nTry to change the RSTUDIO_PANDOC parameter.")
    }
    Sys.setenv(RSTUDIO_PANDOC=RSTUDIO_PANDOC)
    # Run the scripts for all countries.
    # Wrapped in try() statements so that the failure of one country doesn't impact the other countries.
    for (countryiso2 in files){
        # Render the markdown document to html.
        # Note the weird indent is to avoid lines from being truncated when pasted in DOS.
        # Markdown rendering will use the datapath and countryiso2 variables from the global environment.
        try(rmarkdown::render(template,
                              output_file = paste0(outputpath, countryiso2,'.html')))
        # TODO use a try and except stament to keep track of which countries were updated
    }
    cbmio::mergecountrylogs(outputpath)
}


#' Merge log files for all countries in one document called all.md
#' The function will print diagnostic messages to the console if there are errors in the files.
#' The all.md file will contain a table of content with a list of countries
#' linking to the section title of the corresponding country.
#' @param logpath characer path to a folder containing log files for many countries
#' @param allfilename character name of a file
#' @examples
#' \donttest{
#' mergecountrylogs("C:/CBM/cbmlog/upd_aws/")
#' mergecountrylogs("C:/CBM/cbmlog/cbm_update/")
#' mergecountrylogs("C:/CBM/cbmlog/cbm_mws/")
#' }
#' @export
mergecountrylogs <- function(logpath, allfilename = "all.md"){
    allfilename <- file.path(logpath, allfilename)
    if(file.exists(allfilename)){ file.remove(allfilename)}
    files <- list.files(logpath, pattern = ".md", full.names = TRUE)
    files <- files[basename(files) != allfilename]
    countrycodes <- tools::file_path_sans_ext(basename(files))
    # Initialise text variables
    title <- character(0)
    toc <- character(0)
    allcontent <- character(0)
    # Loop to read each country's log file
    # allcontent <- lapply(files, readLines)

    for (f in files){
        content <- readLines(f)
        countrycode <- tools::file_path_sans_ext(basename(f))
        if(any(grepl("Error", content))){
            # Extract the first 50 charcter of the error message for reuse in the country's title
            errorstatus <- substr(content[grepl("Error", content)][1],4,50)
        } else {
            errorstatus <- "ok"
        }
        message(countrycode, ": ", errorstatus)
        # Add country title with anchor for the table of content
        title <- sprintf('<a name="%s"></a>\n\n# %s (%s)\n',
                         countrycode, countrycode, errorstatus)
        date <- content[grepl("date:", content)]
        toc <- c(toc, sprintf('* [%s](#%s) (%s)',
                              countrycode, countrycode, errorstatus))
        # Remove duplicated lines
        content <- content[c(TRUE, !content[-length(content)] == content[-1])]
        # Append the content of the country file to all content
        # The first 12 lines of the content are ignored.
        allcontent <- c(allcontent,
                        title, date, content[-(1:12)])
    }
    # Write all content to a markdown file and render it to html
    writeLines(c(toc, allcontent), allfilename)
    rmarkdown::render(allfilename)
}


#' Extract the first characters of the error message in a log file
#' @param logfile character path to a log file
#' @param n integer number of characters to read
#' @examples
#' \donttest{
#' finderrorstatus("C:/Dev/cbmlog/fusion_aws/CZ.html")
#' finderrorstatus("C:/Dev/cbmlog/cbm_update/CZ.html", n = 10)
#' finderrorstatus("C:/Dev/cbmlog/cbm_update/CZ.html", n = 100)
#' }
#' @return "ok" if there is no error, the first characters of the error message otherwise.
#' @export
finderrorstatus <- function(logfile, n = 50){
    if(!file.exists(logfile)){
        return("-- No log file")
    }
    content <- readLines(logfile)
    if(any(grepl("Error", content))){
        errorstatus <- substr(content[grepl("Error", content)][1], 1, n)
    } else {
        errorstatus <- "OK"
    }
    return(errorstatus)
}


#' Remove consecutive lines that are similar
#' @description \code{removesimilarlines} reduces the size of a log output by removing lines that are similar.
#' To find out similar strings, the input vector is compared to a lagged version of itself
#' and \code{adist} is used to compute the Levenshtein distance between consecutive strings.
#' @param x character vector
#' @param mindistance numeric minimum distance between 2 consecutive strings so
#' that they are kept in the output
#' @examples
#' logsample <- strsplit(" Area 4.7000
#' Sum 1.0000
#' Reading information about 2  1  1 SPU: 2
#' Reading information about 2  1  31 SPU: 2
#' Reading information about 2  1  58 SPU: 2
#' Total Area in SPU 2.0000: 200371.9998
#' Reading information about 2  2  2 SPU: 3",  "\n")[[1]]
#' removesimilarlines(logsample)
#' @export
removesimilarlines <- function(x, mindistance=6){
    d <- Map(adist, x[-length(x)], x[-1])
    return(c(names(d[d>mindistance]),
             x[length(x)]))
}
