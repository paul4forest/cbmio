context("disturbance events")
library(dplyr)

disturbance1 <- data_frame(i = c("a", "a","a", "b", "b"),
                           j = c(1, 1, 2, 1, 3),
                           SWStart = c(1, 21, 21, 41, 101),
                           SWEnd = c(10, 30, 30, 50, 120))

disturbance2 <- data_frame(i = c("a", "a", "a"),
                          SWStart = c(1, 1, 21),
                          SWEnd = c(10, 10, 30),
                          Amount = c(123, 100, 321))

silviculture1 <- data_frame(i = c("a", "a", "b", "b", "c"),
                            j = c(1, 2, 1, 3, 4),
                            Min_age = c(1, 30, 20, 50, 100),
                            Max_age = c(10, 50, 80, 200, 250))

test_that("copydistagerange works", {
    dist1 <- copydistagerange(disturbance1, silviculture1, by = c("i", "j"))
    expect_equal(dist1$SWStart, c(1, 1, 30, 20, 50))
    expect_equal(dist1$SWEnd, c(10, 10, 50, 80, 200))
})

test_that("copydistagerange returns an error if the by argument contains variables that are not in disturbance or silviculture", {
    expect_error(copydistagerange(disturbance1, silviculture1, by = c("SWStart")),
                 "columns in by are not present", ignore.case = TRUE)
})

test_that("copydistagerange returns an error if common variables are missing from the by argument", {
    expect_error(copydistagerange(disturbance1, silviculture1, by = c("i")),
                 "columns are present in both tables", ignore.case = TRUE)
})

test_that("sumdistamount works", {
    dist2 <- sumdistamount(disturbance2)
    expect_equal(dist2$Amount, c(223, 321))
})

test_that("duplicate disturbance works",{
    dist3 <- duplicateandshift(disturbance1, j, 1000)
    expect_equal(dist3$j, c(1, 1, 2, 1, 3, 1001, 1001, 1002, 1001, 1003))
    expect_equal(dist3$SWStart, rep(disturbance1$SWStart, 2))
})
# add1000todisturbancetype <- function(disturbancetype){
