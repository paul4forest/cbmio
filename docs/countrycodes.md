---
title: "Country Codes"
author: "Paul"
date: "August 2, 2018"
output: 
  html_document: 
    keep_md: yes
---





A list of iso2 and iso3 country codes is available from:
https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.csv


Other list available on an Eurostat website:
http://ec.europa.eu/eurostat/statistics-explained/index.php/Glossary:Country_codes
Downloaded and preprocessed in Excel, saved as csv.



Compare sheet names in the file "CBM_harvest_input_data_and_base_year.xlsx"
with the country iso 2 codes provided by Eurostat.



Add country codes from the data folder

|sheet    |countryiso2 |country        |folder |
|:--------|:-----------|:--------------|:------|
|Readme   |Readme      |               |       |
|AT_Const |AT          |Austria        |AT     |
|BE_Const |BE          |Belgium        |BE     |
|BG_Const |BG          |Bulgaria       |BG     |
|CZ_Const |CZ          |Czech Republic |CZ     |
|HR_Const |HR          |Croatia        |HR     |
|DK_Const |DK          |Denmark        |DK     |
|EE_Const |EE          |Estonia        |EE     |
|FI_Const |FI          |Finland        |FI     |
|FR_Const |FR          |France         |FR     |
|DE_Const |DE          |Germany        |DE     |
|EL_Const |EL          |Greece         |       |
|HU_Const |HU          |Hungary        |HU     |
|IE_Const |IE          |Ireland        |IE     |
|IT_Const |IT          |Italy          |IT     |
|LV_Const |LV          |Latvia         |LV     |
|LT_Const |LT          |Lithuania      |LT     |
|LU_Const |LU          |Luxembourg     |LU     |
|NL_Const |NL          |Netherlands    |NL     |
|PL_Const |PL          |Poland         |PL     |
|PT_Const |PT          |Portugal       |PT     |
|RO_Const |RO          |Romania        |RO     |
|SK_Const |SK          |Slovakia       |SK     |
|SI_Const |SI          |Slovenia       |SI     |
|ES_Const |ES          |Spain          |ES     |
|SE_Const |SE          |Sweden         |SE     |
|UK_Const |UK          |United Kingdom |UK     |
|         |CY          |Cyprus         |       |
|         |MT          |Malta          |       |
|         |GR          |               |GR     |

Greece has a folder name "GR" but its Eurostat code is "EL".

