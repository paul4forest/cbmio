Alterfor Upscaling Data
========================================================
author:Paul Rougieux, Joint Research Centre. | Based on data by Roberto Pilli, Sarah Mubareka, Giorgio Vachiano
date: April 4 2019
autosize: true


<!-- Note  Use firefox to print in A4 on landscape orientation -->


Data description 1/2
========================================================
| Name                                      | Filename                       | Description               |
|-------------------------------------------|--------------------------------|---------------------------|
|Share of forest to total land area        | xx\_t1\_area                   | Share of forest according to the NFI (total area taken from NUTS region area computed using a GIS)|
|__Share of forest available for wood supply__ | xx\_t2\_aws                    | Availability for wood supply using the IUCN database.|
| Species diversity                         | xx\_t3\_ft                     | “Forest type” in the CBM database refers to the main species or groups of species reported in the NFI.|
| Species composition                       | xx\_t4\_blcf                   | The fraction of broadleaves and conifers in the forest.|


Data description 2/2
========================================================
| Name                                      | Filename                       | Description               |
|-------------------------------------------|--------------------------------|---------------------------|
|Management System                         | xx\_t5\_mansys                 | Management system reported in the NFI. These are usually not expressive.|
|Management Type                           | xx\_t5\_mantyp                 | Management system reported in the NFI. These are usually not expressive.|
|Climate                                   | xx\_t6\_clim                   | Mean annual temperature and mean annual precipitation for the region<br/> Computed using the climate zone map used in the CBM model.|
|Urbanisation                              | xx\_t7\_pop                    | Population density 2016 from Eurostat|
|Accessibility                             | xx\_t8\_slope                  | Average slope per region|
|__Biomass__                                   | xx\_t9\_mvol               | Merchantable volume per hectare, estimated for 2015 using the CBM model.|


Biomass merchantable volume per hectare
========================================================

|AT       |BE       |BG       |CZ       |DE         |DK       |
|:--------|:--------|:--------|:--------|:----------|:--------|
|AT11_322 |BE20_196 |BG00_158 |CZ01_311 |DE_B-B_315 |DK01_237 |
|AT12_333 |BE30_268 |         |CZ02_331 |DE_B-W_354 |DK02_245 |
|AT13_411 |         |         |CZ03_299 |DE_Bay_357 |DK03_226 |
|AT21_353 |         |         |CZ04_313 |DE_Hes_333 |DK04_225 |
|AT22_339 |         |         |CZ05_325 |DE_M-V_344 |DK05_203 |
|AT31_357 |         |         |CZ06_333 |DE_N-W_330 |         |
|AT32_340 |         |         |CZ07_332 |DE_NHB_336 |         |
|AT33_338 |         |         |CZ08_353 |DE_R-P_350 |         |
|AT34_390 |         |         |         |DE_S-A_326 |         |
|         |         |         |         |DE_S-H_333 |         |
|         |         |         |         |DE_Sac_322 |         |
|         |         |         |         |DE_Sar_320 |         |
|         |         |         |         |DE_Thu_342 |         |
|         |         |         |         |           |         |
|         |         |         |         |           |         |



Biomass merchantable volume per hectare
========================================================




|EE       |ES       |FR       |HR       |HU       |IE       |
|:--------|:--------|:--------|:--------|:--------|:--------|
|EE00_156 |ES11_177 |FR10_124 |HR00_171 |HU00_210 |IE00_206 |
|         |ES12_183 |FR21_139 |         |         |         |
|         |ES13_193 |FR22_123 |         |         |         |
|         |ES21_211 |FR23_115 |         |         |         |
|         |ES22_85  |FR24_130 |         |         |         |
|         |ES23_82  |FR25_111 |         |         |         |
|         |ES24_74  |FR26_141 |         |         |         |
|         |ES30_71  |FR30_117 |         |         |         |
|         |ES41_77  |FR41_152 |         |         |         |
|         |ES42_64  |FR42_134 |         |         |         |
|         |ES43_65  |FR43_152 |         |         |         |
|         |ES51_85  |FR51_128 |         |         |         |
|         |ES52_67  |FR52_134 |         |         |         |
|         |ES53_72  |FR53_139 |         |         |         |
|         |ES61_67  |FR61_126 |         |         |         |
|         |ES62_60  |FR62_124 |         |         |         |
|         |ES70_84  |FR63_145 |         |         |         |
|         |         |FR71_135 |         |         |         |
|         |         |FR72_147 |         |         |         |
|         |         |FR81_110 |         |         |         |
|         |         |FR82_95  |         |         |         |
|         |         |FR83_80  |         |         |         |



Biomass merchantable volume per hectare
========================================================


|IT      |LT       |LU       |LV       |NL       |PL       |
|:-------|:--------|:--------|:--------|:--------|:--------|
|AA_201  |LT00_163 |LU00_231 |LV00_208 |NL00_213 |PL00_252 |
|Ab_141  |         |         |         |         |         |
|Ba_141  |         |         |         |         |         |
|Cal_158 |         |         |         |         |         |
|Cam_145 |         |         |         |         |         |
|ER_149  |         |         |         |         |         |
|FVG_203 |         |         |         |         |         |
|La_163  |         |         |         |         |         |
|Li_158  |         |         |         |         |         |
|Lo_188  |         |         |         |         |         |
|Ma_147  |         |         |         |         |         |
|Mo_108  |         |         |         |         |         |
|Pi_168  |         |         |         |         |         |
|Pu_108  |         |         |         |         |         |
|Sa_72   |         |         |         |         |         |
|Si_107  |         |         |         |         |         |
|Tn_192  |         |         |         |         |         |
|To_181  |         |         |         |         |         |
|Um_141  |         |         |         |         |         |
|Va_137  |         |         |         |         |         |
|Ve_182  |         |         |         |         |         |
|        |         |         |         |         |         |




Biomass merchantable volume per hectare
========================================================



|PT       |RO       |SE       |SI       |SK       |UK       |
|:--------|:--------|:--------|:--------|:--------|:--------|
|PT11_102 |RO11_203 |SE11_187 |SL00_262 |SK00_279 |UKE0_145 |
|PT15_117 |RO12_203 |SE12_186 |         |         |UKL0_168 |
|PT16_111 |RO21_211 |SE21_188 |         |         |UKM0_152 |
|PT17_138 |RO22_216 |SE22_189 |         |         |UKN0_143 |
|PT18_106 |RO31_217 |SE23_188 |         |         |         |
|         |RO41_195 |SE31_186 |         |         |         |
|         |RO42_197 |SE32_167 |         |         |         |
|         |         |SE33_132 |         |         |         |


<!-- Slide With Code -->
<!-- ======================================================== -->

<!-- ```{r} -->
<!-- summary(cars) -->
<!-- ``` -->


<!-- Slide With Plot -->
<!-- ======================================================== -->

<!-- ```{r, echo=FALSE} -->
<!-- plot(cars) -->
<!-- ``` -->
<!-- First Slide -->
<!-- ======================================================== -->

<!-- - Bullet 1 -->
<!-- - Bullet 2 -->
<!-- - Bullet 3 -->

