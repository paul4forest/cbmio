---
title: "Age Class Distribution"
author: "Paul"
date: "July 30, 2018"
output:
  html_document: 
    keep_md: yes
    toc: yes
---




```r
datapath <- "C:/CBM/data"
datapathcountry <- file.path(datapath, "SI")
```

This document takes age class distribution data from the access database.

# Read input data 

## Data in the AccessDB located in the folder "from_cbm_calibrartion" 


```r
accessdbcalib <- RODBC::odbcDriverConnect(sprintf("Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=%s/from_CBM_calibration/SL_2000_Const_V.mdb", datapathcountry))
accessdbcalibtables <- RODBC::sqlTables(accessdbcalib)
backinventory <- RODBC::sqlFetch(accessdbcalib, "BACK_inventory",  stringsAsFactors = FALSE)
Ave_Age_Evolution_qry <- RODBC::sqlFetch(accessdbcalib, "Ave_Age_Evolution_qry",  stringsAsFactors = FALSE)
RODBC::odbcCloseAll() 

# Change Age to a factor, ordered correctly
agefactor1 <- backinventory %>% distinct(Age) %>% mutate(n = as.integer(gsub("AGEID","",Age))) %>% arrange(n)
backinventory <- backinventory %>% mutate(Age = factor(Age, levels = agefactor1$Age))
agefactor2 <- Ave_Age_Evolution_qry %>% 
    distinct(Age) %>% 
    mutate(id = gsub("AGEID","ID",Age), # Create shorter id so it's visible on bar plots
           n = as.integer(gsub("AGEID","",Age))) %>% arrange(n)
Ave_Age_Evolution_qry<- Ave_Age_Evolution_qry %>% 
    mutate(Age = factor(Age, levels = agefactor2$Age, labels = agefactor2$id))
```
There are 113 tables in this database!

Todo: get the number of columns and rows for each table.

## Data in the AccessDB located in the result folder "2015/Makelist_output" 

```r
accessdb2015output <- RODBC::odbcDriverConnect(sprintf("Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=%s/2015/Makelist_output/cbm_project.mdb", datapathcountry))
accessdb2015outputtables <- RODBC::sqlTables(accessdb2015output)
tblInventory <- RODBC::sqlFetch(accessdb2015output, "tblInventory")
RODBC::odbcCloseAll() 
```
174 tables in this database!




# Plot Age class distribution 

## Plot based on data from "SI/from_CBM_calibration/SL_2000_Const_V.mdb"
### In the table "backinventory" 

```r
ggplot(data=backinventory, aes(x = Age, y = Area/1e3)) +
    geom_bar(stat="identity", fill="darkgreen") + 
    ylab("Area in 1000ha")
```

![](ageclassdistribution_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

### In the table "Ave_Age_Evolution_qry"

```r
ggplot(data = Ave_Age_Evolution_qry, aes(x = Age, y = Area/1e3)) +
    geom_bar(stat="identity", fill="darkgreen") + 
    ylab("Area in 1000ha")
```

![](ageclassdistribution_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

Other variables in the "Ave_Age_Evolution_qry" table:


```r
ggplot(data = Ave_Age_Evolution_qry, aes(x = Age, y = Biomass_Carbon_ha)) +
    geom_bar(stat="identity", fill="darkgreen")
```

![](ageclassdistribution_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

```r
ggplot(data = Ave_Age_Evolution_qry, aes(x = Age, y = Merch_C_ha)) +
    geom_bar(stat="identity", fill="darkgreen")
```

![](ageclassdistribution_files/figure-html/unnamed-chunk-5-2.png)<!-- -->

```r
ggplot(data = Ave_Age_Evolution_qry, aes(x = Age, y = Merch_Vol_ha)) +
    geom_bar(stat="identity", fill="darkgreen")
```

![](ageclassdistribution_files/figure-html/unnamed-chunk-5-3.png)<!-- -->




## Plot based on data from SI/2015/Makelist_output/cbm_project.mdb 
Barplot with ggplot

```r
ggplot(data=tblInventory, aes(x = Age, y = Area/1e3)) +
    geom_bar(stat="identity", fill="darkgreen") + 
    ylab("Area in 1000ha")
```

![](ageclassdistribution_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

```r
# Sum age and area
tblInventorysum <- tblInventory %>%
    group_by(Age) %>% 
    summarise(Area = sum(Area))
ggplot(data=tblInventorysum, aes(x = Age, y = Area/1e3)) +
    geom_bar(stat="identity", fill="darkgreen") + 
    ylab("Area in 1000ha")
```

![](ageclassdistribution_files/figure-html/unnamed-chunk-6-2.png)<!-- -->

Barplot in ascii mode with txt plot

```
Area in 1000 ha as a function of the Age
```

```
   +-+--------------+--------------+-------------+---------+
25 +                         ****                          +
   |             ****  ****                                |
20 +                               ****                    +
   |                                                       |
   |                                                       |
15 +                                                       +
   |       ****                                            |
10 +                                     ****              +
   |                                                       |
   |                                                       |
 5 +                                           ***         +
   | ****                                            ***   |
 0 +-+--------------+--------------+-------------+---------+
     0             50             100           150         
```


