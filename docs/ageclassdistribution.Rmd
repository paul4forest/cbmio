---
title: "Age Class Distribution"
author: "Paul"
date: "July 30, 2018"
output:
  html_document:
    keep_md: yes
    toc: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = "..") # file path relative to the root of the project directory
library(ggplot2)
library(dplyr)
```

```{r loaddatapath}
datapath <- "C:/Users/mubarsa/ownCloud/SourceData/EFDM/CBM/"
datapathcountry <- file.path(datapath, "SI")
```

This document takes age class distribution data from the access database.

# Read input data 

Age class distribution is available in 

* "Ave_Age_Evolution_qry" check that the Timestop for example 16 in the case of SE
is correctly set.
* Another source is the harvest data in "HWP_Analysis" check time step 16 for SE for example

## Data in the AccessDB located in the folder "from_cbm_calibrartion" 
```{r}
accessdbcalib <- RODBC::odbcDriverConnect(sprintf("Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=%s/from_CBM_calibration/SL_2000_Const_V.mdb", datapathcountry))
accessdbcalibtables <- RODBC::sqlTables(accessdbcalib)
backinventory <- RODBC::sqlFetch(accessdbcalib, "BACK_inventory",  stringsAsFactors = FALSE)
Ave_Age_Evolution_qry <- RODBC::sqlFetch(accessdbcalib, "Ave_Age_Evolution_qry",  stringsAsFactors = FALSE)
RODBC::odbcCloseAll() 

# Change Age to a factor, ordered correctly
agefactor1 <- backinventory %>% distinct(Age) %>% mutate(n = as.integer(gsub("AGEID","",Age))) %>% arrange(n)
backinventory <- backinventory %>% mutate(Age = factor(Age, levels = agefactor1$Age))
agefactor2 <- Ave_Age_Evolution_qry %>% 
    distinct(Age) %>% 
    mutate(id = gsub("AGEID","ID",Age), # Create shorter id so it's visible on bar plots
           n = as.integer(gsub("AGEID","",Age))) %>% arrange(n)
Ave_Age_Evolution_qry<- Ave_Age_Evolution_qry %>% 
    mutate(Age = factor(Age, levels = agefactor2$Age, labels = agefactor2$id))
```
There are 113 tables in this database!

Todo: get the number of columns and rows for each table.

## Data in the AccessDB located in the result folder "2015/Makelist_output" 
```{r}
accessdb2015output <- RODBC::odbcDriverConnect(sprintf("Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=%s/2015/Makelist_output/cbm_project.mdb", datapathcountry))
accessdb2015outputtables <- RODBC::sqlTables(accessdb2015output)
tblInventory <- RODBC::sqlFetch(accessdb2015output, "tblInventory")
RODBC::odbcCloseAll() 
```
174 tables in this database!




# Plot Age class distribution 

## Plot based on data from "SI/from_CBM_calibration/SL_2000_Const_V.mdb"
### In the table "backinventory" 
```{r}
ggplot(data=backinventory, aes(x = Age, y = Area/1e3)) +
    geom_bar(stat="identity", fill="darkgreen") + 
    ylab("Area in 1000ha")
```

### In the table "Ave_Age_Evolution_qry"
```{r fig.width=12}
ggplot(data = Ave_Age_Evolution_qry, aes(x = Age, y = Area/1e3)) +
    geom_bar(stat="identity", fill="darkgreen") + 
    ylab("Area in 1000ha")
```

Other variables in the "Ave_Age_Evolution_qry" table:

```{r fig.width=12}
ggplot(data = Ave_Age_Evolution_qry, aes(x = Age, y = Biomass_Carbon_ha)) +
    geom_bar(stat="identity", fill="darkgreen")
ggplot(data = Ave_Age_Evolution_qry, aes(x = Age, y = Merch_C_ha)) +
    geom_bar(stat="identity", fill="darkgreen")
ggplot(data = Ave_Age_Evolution_qry, aes(x = Age, y = Merch_Vol_ha)) +
    geom_bar(stat="identity", fill="darkgreen")
```




## Plot based on data from SI/2015/Makelist_output/cbm_project.mdb 
Barplot with ggplot
```{r}
ggplot(data=tblInventory, aes(x = Age, y = Area/1e3)) +
    geom_bar(stat="identity", fill="darkgreen") + 
    ylab("Area in 1000ha")
# Sum age and area
tblInventorysum <- tblInventory %>%
    group_by(Age) %>% 
    summarise(Area = sum(Area))
ggplot(data=tblInventorysum, aes(x = Age, y = Area/1e3)) +
    geom_bar(stat="identity", fill="darkgreen") + 
    ylab("Area in 1000ha")
```

Barplot in ascii mode with txt plot
```{r comment="", echo=FALSE}
# ggplot(data=tblInventory, aes(x = Age, y = Area/1e3)) +
#     geom_bar(stat="identity", fill="darkgreen") + 
#     ylab("Area in 1000ha")

message("Area in 1000 ha as a function of the Age")
txtplot::txtplot(tblInventorysum$Age, tblInventorysum$Area/1e3)
# txtplot::txtbarchart()
```

## Plot based on the cbm calibration files (age class distribution input)

Change Age to a factor

```{r fig.width=12}

# Load the data
inventoryhuie <- readcbm("BACK_inventory", datapath, c("HU", "IE")) %>% 
    ungroup() %>% 
    select(-path, -accessfilepath) 

# replace the _7 column with cl7
inventoryhuie$cl7 <- inventoryhuie[,"_7"][[1]]

# Change Age to a factor, ordered correctly
agefactor1 <- inventoryhuie %>% distinct(Age) %>% mutate(n = as.integer(gsub("AGEID","",Age))) %>% arrange(n)
inventoryhuie <- inventoryhuie %>% 
    mutate(Age = factor(Age, levels = agefactor1$Age))

# Plot
p <- ggplot(data=inventoryhuie, aes(x = Age, y = Area/1e3)) +
    geom_bar(stat="identity", fill="darkgreen") + 
    ylab("Area in 1000ha") 
p +  facet_grid(countryiso2~.)
p + facet_grid(countryiso2 ~ cl7)
```




#  AgeClassLookup table 

The `AgeClassLookup` table is necessary for some of the queries performed by CBM. 
A join with the `AgeClassLookup` table is used for example by the query that generates
`inventory2015.csv` at the end of `cbm_update.py`.



The file will be storred in the cbmio package and written to `AgeClassLookup.csv` 
at the end of the expandFaws.R script.

```{r}
system.file("AgeClassLookup.csv", package="cbmio")

# CZ AgeClassLookup.csv file exists but not the DE file
file.exists(file.path(datapath, "CZ", "AgeClassLookup.csv"))
AgeClassLookupPath <- file.path(datapath, "DE", "AgeClassLookup.csv")
file.exists(AgeClassLookupPath)
file.copy(system.file("AgeClassLookup.csv", package="cbmio"),
          AgeClassLookupPath)
file.exists(AgeClassLookupPath)
```


