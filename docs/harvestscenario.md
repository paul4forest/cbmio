---
title: "Age Class Distribution"
author: "Paul"
date: "July 30, 2018"
output:
  html_document:
    keep_md: yes
    toc: yes
---



This document takes age class distribution data from the access database.


# Read input data 

Harvest data is available in:
* an Excel file at the root of the CBM directory
* another source is the harvest data in "HWP_Analysis" check time step 16 for SE for example
* at the end of a `cbm_run.py`, harvest data should be available in diagnostic tables


## Data in the Excel file "CBM_harvest_input_data_and_base_year.xlsx"
Load harvest data
Read all sheets and extract the base year

```r
path <- file.path(datapath, "CBM_harvest_input_data_and_base_year.xlsx")
# Read all sheets the lapply way
# harvest <- lapply(readxl::excel_sheets(path), readxl::read_excel, path = path)
#
# Read all sheets the purr way, keep sheet names as a variable and store data in a list-column
harvestnested <- data_frame(sheet = readxl::excel_sheets(path)) %>% 
    mutate(countryiso2 = gsub("_Const","", sheet),
           path = path) %>% 
    filter(sheet != "Readme") %>% 
    mutate(data = map2(path, sheet, readxl::read_excel)) 
names(harvestnested) <- tolower(names(harvestnested))

harvest <- harvestnested %>% 
    unnest() %>% 
    select(-sheet, -path) %>% 
    rename_all(tolower)

# Check total
harvest %>% 
    mutate(total2 = irw_c + irw_b + fw_c + fw_b,
           diff = total - total2) %>% 
    summarise(diff = sum(abs(diff)))
```

```
## # A tibble: 1 x 1
##          diff
##         <dbl>
## 1 0.000000456
```

```r
firstsheet <- readxl::read_excel(path, "Readme", skip = 6)
```


### Plot harvest


```r
harvestl <- harvest %>% 
    select(-total) %>% 
    gather(product, volume, -countryiso2, -step, -years)
ggplot(harvestl, aes(x = years, y = volume/1e6)) +
    geom_line() +
    ylab("Harvest volume in million m3") +
    facet_grid(countryiso2 ~ product, scales = "free_y")
```

![](harvestscenario_files/figure-html/unnamed-chunk-2-1.png)<!-- -->



## Calibration Data in the AccessDB located in the folder "from_cbm_calibrartion" 


```r
accessdbcalib <- RODBC::odbcDriverConnect(sprintf("Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=%s/from_CBM_calibration/SL_2000_Const_V.mdb", datapathcountry))
accessdbcalibtables <- RODBC::sqlTables(accessdbcalib)
hwpanalysis <- RODBC::sqlFetch(accessdbcalib, "HWP_ANALYSIS",  stringsAsFactors = FALSE)
RODBC::odbcCloseAll() 
# Reshape the harvest data in long format, create shorter product names and add years to the time steps
names(hwpanalysis) <- tolower(names(hwpanalysis))
harvestsi <- hwpanalysis %>% 
    gather(product, volume, -timestep) %>% 
    mutate(product = gsub("vol_merch_", "", product),
           product = gsub("tot_vol_", "", product),
           year = baseyear + timestep - 1) 
```


### Plot harvest time series for SI


```r
ggplot(harvestsi, aes(x =  year, y = volume/1e6)) +
    geom_line() +
    ylab("Harvest volume in million m3") +
    facet_wrap(~product)
```

![](harvestscenario_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

