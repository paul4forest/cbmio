---
title: "Read data from CBM access files"
author: "Paul"
date: "August 8, 2018"
output: 
  html_document: 
    keep_md: yes
    toc: yes
---





# Introduction

Read data from many access files, for all European coutnries. 
The function will be vectorised on country code, it will accept many country codes and return the Access data. 

The function will check if there is a unique access files matching the given country %s and pattern %s.


# Develop the readcbmnested() function


```r
# Define arguments here
datapath <- "C:/CBM/data"
table <- "Dist_Events_Const" # c("BACK_inventory", "Ave_Age_Evolution_qry")
subfolder = "from_CBM_calibration"
pattern = ".*Const.*mdb$"
# countryiso2 <- c("FR", "DE")
countryiso2 <- NULL

if (!is_null(countryiso2)){
    # If a vector of countryiso2 codes is given:
    # Create a dataframe with their folder path.
    # Check that the paths exist for each country code.
    countryfolders <- data_frame(path = file.path(datapath, countryiso2),
                                 countryiso2 = countryiso2)
    stopifnot(file.exists(countryfolders$path))
} else { 
    # If countryiso2 is not given (the default):
    # Discover the name of all 2 digit folders in data path 
    # Get the list of coutnry iso 2 codes in data path.
    countryfolders <- data_frame(path = list.files(datapath, full.names = TRUE)) %>% 
        mutate(countryiso2 = basename(path)) %>% 
        filter(nchar(countryiso2) ==2) # keep only folder names containing a 2 letter country code
}

accessfiles <- countryfolders %>% 
    # Find the access file names based on the default pattern given in findfilepath
    mutate(accessfilepath = map(path, findfilepath, 
                                subfolder = subfolder, pattern = pattern)) %>% 
    group_by(countryiso2, path) %>% 
    # Check the number of files for each country (should be one only)
    mutate(nfile = as_vector(map(accessfilepath, length))) %>%
    unnest()

if(any(as_vector(accessfiles$nfile)>1)){
    warning("These folders have more than one access file:")
    accessfiles %>% filter(nfile>1) %>% unnest() %>%  knitr::kable(format = "markdown") %>% print()
}


# Load the data 
cbmdata <- accessfiles %>% 
    mutate(data = map(accessfilepath, readaccesstable, table = table)) 
```


# Develop the checkcolumns function

## Check columns in the inventory tables

Check if column names are identicall for all countries in the %s tables

If there is an issue, display the number of columns per country and their names.

In case of differing column names,",
you can load all countries that have the same column names by specifying their code with the argument :
`countryiso2 = dput(countryiso2)`


```r
# inventoryitdenested <- readcbmnested("BACK_inventory", "C:/CBM/data", c("IT","DE"))
# inventoryitdenested %>% checkcolumns()

inventorynested <- readcbmnested("BACK_inventory", "C:/CBM/data")
```

```
## Loading BACK_inventory from C:/CBM/data/AT/from_CBM_calibration/AT_1998_Const_MAKER_IV_2c.mdb.
```

```
##     11033 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/BE/from_CBM_calibration/BE_1999_MAKER_Const_3.mdb.
```

```
##     323 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/BG/from_CBM_calibration/BG_2010_MAKER_Const_6.mdb.
```

```
##     2603 rows, 15 columns
```

```
## Loading BACK_inventory from C:/CBM/data/CZ/from_CBM_calibration/CZ_2000_MAKER_Const_IV.mdb.
```

```
##     3563 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/DE/from_CBM_calibration/DE_1992_Const_V.mdb.
```

```
##     5586 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/DK/from_CBM_calibration/DK_1994_MAKER_Const_IV.mdb.
```

```
##     617 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/EE/from_CBM_calibration/EE_2000_MAKER_Const_IV.mdb.
```

```
##     244 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/ES/from_CBM_calibration/ES_1992_Const_VII.mdb.
```

```
##     828 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/FI/from_CBM_calibration/FI_1999_MAKER_Const_IV.mdb.
```

```
##     720 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/FR/from_CBM_calibration/FR_1998_Const_VI.mdb.
```

```
##     5213 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/GR/from_CBM_calibration/GR_1992_MAKER_Const_IVb.mdb.
```

```
##     566 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/HR/from_CBM_calibration/HR_1995_MAKER_Const_IVb.mdb.
```

```
##     1353 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/HU/from_CBM_calibration/HU_2015_MAKER_Const_9_IV_FUSION.mdb.
```

```
##     524 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/IE/from_CBM_calibration/IE_2005_FMAR_Const_VIII.mdb.
```

```
##     272 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/IT/from_CBM_calibration/IT_1995_Const_IV.mdb.
```

```
##     16495 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/LT/from_CBM_calibration/LIT_1996_Const_IV.mdb.
```

```
##     238 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/LU/from_CBM_calibration/LUX_1999_Const_V.mdb.
```

```
##     100 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/LV/from_CBM_calibration/LV_2000_Const_IV.mdb.
```

```
##     170 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/NL/from_CBM_calibration/NL_1997_MAKER_Const_V.mdb.
```

```
##     77 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/PL/from_CBM_calibration/PL_2010_Const_4e.mdb.
```

```
##     10485 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/PT/from_CBM_calibration/PT_1995_MAKER_Const_FM_AR_VI_300.mdb.
```

```
##     1692 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/RO/from_CBM_calibration/RO_2010_MAKER_Const_Vb.mdb.
```

```
##     3761 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/SE/from_CBM_calibration/SW_1996_Const.mdb.
```

```
##     2037 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/SI/from_CBM_calibration/SL_2000_Const_V.mdb.
```

```
##     1008 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/SK/from_CBM_calibration/SK_2010_MAKER_Const_2.mdb.
```

```
##     1887 rows, 14 columns
```

```
## Loading BACK_inventory from C:/CBM/data/UK/from_CBM_calibration/UK_1997_Const_IV.mdb.
```

```
##     1860 rows, 14 columns
```

```r
# function content pasted here
inventorynested %>% 
    ungroup() %>% 
    mutate(ncol = map_int(data, ncol),
           colnames = map(data, names),
           colnames = map_chr(colnames, paste, collapse = ", "))  %>% 
    select(countryiso2, ncol, colnames) 
```

```
## # A tibble: 26 x 3
##    countryiso2  ncol colnames                                             
##    <chr>       <int> <chr>                                                
##  1 AT             14 _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Dela~
##  2 BE             14 _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Dela~
##  3 BG             15 _1, _2, _3, _4, _5, _6, _7, _8, UsingID, Age, Area, ~
##  4 CZ             14 _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Dela~
##  5 DE             14 _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Dela~
##  6 DK             14 _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Dela~
##  7 EE             14 _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Dela~
##  8 ES             14 _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Dela~
##  9 FI             14 _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Dela~
## 10 FR             14 _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Dela~
## # ... with 16 more rows
```

```r
# With grouped colnames
inventorynested %>% 
        ungroup() %>% 
    mutate(ncol = map_int(data, ncol),
           colnames = map(data, names),
           colnames = map_chr(colnames, paste, collapse = ", "))  %>% 
    select(countryiso2, ncol, colnames) %>% 
    group_by(ncol, colnames) %>% 
    summarise(countryiso2 = paste(countryiso2, collapse = ", ")) %>% 
    select(countryiso2, ncol, colnames) %>% 
    knitr::kable(format = "markdown")
```



|countryiso2                                                                                    | ncol|colnames                                                                               |
|:----------------------------------------------------------------------------------------------|----:|:--------------------------------------------------------------------------------------|
|AT, CZ, DE, DK, EE, ES, FI, FR, GR, HR, HU, IE, IT, LT, LU, LV, NL, PL, PT, RO, SE, SI, SK, UK |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|BE                                                                                             |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, LastDist, HistDist     |
|BG                                                                                             |   15|_1, _2, _3, _4, _5, _6, _7, _8, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist |

```r
# use the function
inventorynested %>% checkcolumns(collapsecountry = FALSE) %>% knitr::kable(format = "markdown")
```



|countryiso2 | ncol|colnames                                                                               |
|:-----------|----:|:--------------------------------------------------------------------------------------|
|AT          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|BE          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, LastDist, HistDist     |
|BG          |   15|_1, _2, _3, _4, _5, _6, _7, _8, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist |
|CZ          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|DE          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|DK          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|EE          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|ES          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|FI          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|FR          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|GR          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|HR          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|HU          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|IE          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|IT          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|LT          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|LU          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|LV          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|NL          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|PL          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|PT          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|RO          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|SE          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|SI          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|SK          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|UK          |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |

```r
inventorynested %>% checkcolumns(collapsecountry = TRUE) %>% knitr::kable(format = "markdown")
```



|countryiso2                                                                                    | ncol|colnames                                                                               |
|:----------------------------------------------------------------------------------------------|----:|:--------------------------------------------------------------------------------------|
|AT, CZ, DE, DK, EE, ES, FI, FR, GR, HR, HU, IE, IT, LT, LU, LV, NL, PL, PT, RO, SE, SI, SK, UK |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist     |
|BE                                                                                             |   14|_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, LastDist, HistDist     |
|BG                                                                                             |   15|_1, _2, _3, _4, _5, _6, _7, _8, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist |


## Check columns in the Ave_Age_Evolution_qry tables

```r
aveageevolutionnested <- readcbmnested("Ave_Age_Evolution_qry", "C:/CBM/data")
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/AT/from_CBM_calibration/AT_1998_Const_MAKER_IV_2c.mdb.
```

```
##     7204 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/BE/from_CBM_calibration/BE_1999_MAKER_Const_3.mdb.
```

```
## 42S02 -1305 [Microsoft][ODBC Microsoft Access Driver] The Microsoft Access database engine cannot find the input table or query 'tblAgeClasses'. Make sure it exists and that its name is spelled correctly.[RODBC] ERROR: Could not SQLExecDirect 'SELECT * FROM "Ave_Age_Evolution_qry"'
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/BG/from_CBM_calibration/BG_2010_MAKER_Const_6.mdb.
```

```
##     1626 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/CZ/from_CBM_calibration/CZ_2000_MAKER_Const_IV.mdb.
```

```
##     1828 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/DE/from_CBM_calibration/DE_1992_Const_V.mdb.
```

```
##     3444 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/DK/from_CBM_calibration/DK_1994_MAKER_Const_IV.mdb.
```

```
##     408 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/EE/from_CBM_calibration/EE_2000_MAKER_Const_IV.mdb.
```

```
##     128 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/ES/from_CBM_calibration/ES_1992_Const_VII.mdb.
```

```
## 42S02 -1305 [Microsoft][ODBC Microsoft Access Driver] The Microsoft Access database engine cannot find the input table or query 'tblAgeClasses'. Make sure it exists and that its name is spelled correctly.[RODBC] ERROR: Could not SQLExecDirect 'SELECT * FROM "Ave_Age_Evolution_qry"'
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/FI/from_CBM_calibration/FI_1999_MAKER_Const_IV.mdb.
```

```
##     323 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/FR/from_CBM_calibration/FR_1998_Const_VI.mdb.
```

```
##     4596 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/GR/from_CBM_calibration/GR_1992_MAKER_Const_IVb.mdb.
```

```
##      rows,  columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/HR/from_CBM_calibration/HR_1995_MAKER_Const_IVb.mdb.
```

```
##     806 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/HU/from_CBM_calibration/HU_2015_MAKER_Const_9_IV_FUSION.mdb.
```

```
##     294 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/IE/from_CBM_calibration/IE_2005_FMAR_Const_VIII.mdb.
```

```
##     314 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/IT/from_CBM_calibration/IT_1995_Const_IV.mdb.
```

```
##     11256 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/LT/from_CBM_calibration/LIT_1996_Const_IV.mdb.
```

```
##     159 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/LU/from_CBM_calibration/LUX_1999_Const_V.mdb.
```

```
##     111 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/LV/from_CBM_calibration/LV_2000_Const_IV.mdb.
```

```
##     100 rows, 15 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/NL/from_CBM_calibration/NL_1997_MAKER_Const_V.mdb.
```

```
##     86 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/PL/from_CBM_calibration/PL_2010_Const_4e.mdb.
```

```
##     4483 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/PT/from_CBM_calibration/PT_1995_MAKER_Const_FM_AR_VI_300.mdb.
```

```
##     1201 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/RO/from_CBM_calibration/RO_2010_MAKER_Const_Vb.mdb.
```

```
##     2490 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/SE/from_CBM_calibration/SW_1996_Const.mdb.
```

```
##     1187 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/SI/from_CBM_calibration/SL_2000_Const_V.mdb.
```

```
##     1078 rows, 19 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/SK/from_CBM_calibration/SK_2010_MAKER_Const_2.mdb.
```

```
##     1082 rows, 20 columns
```

```
## Loading Ave_Age_Evolution_qry from C:/CBM/data/UK/from_CBM_calibration/UK_1997_Const_IV.mdb.
```

```
##     1676 rows, 20 columns
```

```r
aveageevolutionnested %>% 
    checkcolumns(collapsecountry = FALSE) %>% 
    knitr::kable(format = "markdown")
```

```
## Warning in checkcolumns(., collapsecountry = FALSE): Some datasets are
## empty, this can cause errors.You might want to filter them out with
## map_lgl(data, is.null).
```



|countryiso2 |ncol |colnames                                                                                                                                                   |
|:-----------|:----|:----------------------------------------------------------------------------------------------------------------------------------------------------------|
|AT          |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, Expr1, Merch_C_ha, Merch_Vol_ha, DB, TimeStep   |
|BG          |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB, TimeStep |
|CZ          |19   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB           |
|DE          |19   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB               |
|DK          |19   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB           |
|EE          |19   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB           |
|FI          |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB, TimeStep |
|FR          |19   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB           |
|HR          |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB, TimeStep |
|HU          |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, Expr1, Merch_C_ha, Merch_Vol_ha, DB, TimeStep   |
|IE          |20   |TimeStep, _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB     |
|IT          |19   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB               |
|LT          |20   |TimeStep, _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB     |
|LU          |19   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB               |
|LV          |15   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha                                                      |
|NL          |19   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB           |
|PL          |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB, TimeStep |
|PT          |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, Expr1, Merch_C_ha, Merch_Vol_ha, DB, TimeStep   |
|RO          |19   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB           |
|SE          |19   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB               |
|SI          |19   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB               |
|SK          |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB, TimeStep |
|UK          |20   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB, TimeStep     |
|BE          |NA   |NA                                                                                                                                                         |
|ES          |NA   |NA                                                                                                                                                         |
|GR          |NA   |NA                                                                                                                                                         |

```r
aveageevolutionnested %>% 
    checkcolumns(collapsecountry = TRUE) %>% 
    knitr::kable(format = "markdown")
```

```
## Warning in checkcolumns(., collapsecountry = TRUE): Some datasets are
## empty, this can cause errors.You might want to filter them out with
## map_lgl(data, is.null).
```



|countryiso2            |ncol |colnames                                                                                                                                                   |
|:----------------------|:----|:----------------------------------------------------------------------------------------------------------------------------------------------------------|
|LV                     |15   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha                                                      |
|DE, IT, LU, SE, SI     |19   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB               |
|CZ, DK, EE, FR, NL, RO |19   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB           |
|UK                     |20   |_1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB, TimeStep     |
|BG, FI, HR, PL, SK     |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB, TimeStep |
|AT, HU, PT             |20   |Status, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, Expr1, Merch_C_ha, Merch_Vol_ha, DB, TimeStep   |
|IE, LT                 |20   |TimeStep, _1, _2, _3, _4, _5, _6, _7, UsingID, Age, Area, Delay, UNFCCCL, HistDist, LastDist, Biomass_Carbon_ha, BEF_Tot, Merch_C_ha, Merch_Vol_ha, DB     |
|BE, ES, GR             |NA   |NA                                                                                                                                                         |

```r
# data used to contain:
# [[1]]
# [1] "42S02 -1305 [Microsoft][ODBC Microsoft Access Driver] The Microsoft Access database engine cannot find the input table or query 'tblAgeClasses'. Make sure it exists and that its name is spelled correctly."
# [2] "[RODBC] ERROR: Could not SQLExecDirect 'SELECT * FROM \"Ave_Age_Evolution_qry\"'"
# The readaccesstable() function has now beeen changed to return NULL in all cases.
# It prints a warning if there is no data. 
# Distinguish between the bulgarian and the belgian issue
# Bulgaria : no data at all, data is null
# Belgium data is equal to a character string giving a connection error
```

Compare column names

```r
# load the most frequent one
aae <- aveageevolutionnested %>% 
    checkcolumns(collapsecountry = TRUE) %>% 
    mutate(nc = nchar(countryiso2)) %>% 
    ungroup()
```

```
## Warning in checkcolumns(., collapsecountry = TRUE): Some datasets are
## empty, this can cause errors.You might want to filter them out with
## map_lgl(data, is.null).
```

```r
mostfrequent <- filter(aae, nc == max(nc))

# diff based on https://stackoverflow.com/questions/28834459/extract-characters-that-differ-between-two-strings

main <- mostfrequent$colnames
first <- aae$colnames[1]
reduce(strsplit(c(first, main), split = ","), setdiff)
```

```
## [1] "_1"
```

```r
reduce(strsplit(c(main, first), split = ","), setdiff)
```

```
## [1] "Status"        " BEF_Tot"      " Merch_C_ha"   " Merch_Vol_ha"
## [5] " DB"
```

```r
aae2 <- aae %>% 
    mutate(two = map(colnames, c, main),
        colsplit = map(two, strsplit, split = ", "),
        diff = reduce(colsplit, setdiff),
        diff1 = paste(reduce(strsplit(c(main, colnames), split = ","), setdiff), collapse = ", ")) 
```


## Check columns in the Disturbance Events tables


```r
disteventsnested <-  readcbmnested("Dist_Events_Const", "C:/CBM/data")
```

```
## Loading Dist_Events_Const from C:/CBM/data/AT/from_CBM_calibration/AT_1998_Const_MAKER_IV_2c.mdb.
```

```
##     10657 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/BE/from_CBM_calibration/BE_1999_MAKER_Const_3.mdb.
```

```
##     2550 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/BG/from_CBM_calibration/BG_2010_MAKER_Const_6.mdb.
```

```
##     5927 rows, 40 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/CZ/from_CBM_calibration/CZ_2000_MAKER_Const_IV.mdb.
```

```
##     2912 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/DE/from_CBM_calibration/DE_1992_Const_V.mdb.
```

```
##     13839 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/DK/from_CBM_calibration/DK_1994_MAKER_Const_IV.mdb.
```

```
##     2223 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/EE/from_CBM_calibration/EE_2000_MAKER_Const_IV.mdb.
```

```
##     1462 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/ES/from_CBM_calibration/ES_1992_Const_VII.mdb.
```

```
##     5546 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/FI/from_CBM_calibration/FI_1999_MAKER_Const_IV.mdb.
```

```
##     1442 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/FR/from_CBM_calibration/FR_1998_Const_VI.mdb.
```

```
##     19553 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/GR/from_CBM_calibration/GR_1992_MAKER_Const_IVb.mdb.
```

```
##     866 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/HR/from_CBM_calibration/HR_1995_MAKER_Const_IVb.mdb.
```

```
##     3751 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/HU/from_CBM_calibration/HU_2015_MAKER_Const_9_IV_FUSION.mdb.
```

```
##     4918 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/IE/from_CBM_calibration/IE_2005_FMAR_Const_VIII.mdb.
```

```
##     7117 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/IT/from_CBM_calibration/IT_1995_Const_IV.mdb.
```

```
##     18023 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/LT/from_CBM_calibration/LIT_1996_Const_IV.mdb.
```

```
##     3666 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/LU/from_CBM_calibration/LUX_1999_Const_V.mdb.
```

```
##     1899 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/LV/from_CBM_calibration/LV_2000_Const_IV.mdb.
```

```
##     2491 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/NL/from_CBM_calibration/NL_1997_MAKER_Const_V.mdb.
```

```
##     2506 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/PL/from_CBM_calibration/PL_2010_Const_4e.mdb.
```

```
##     26904 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/PT/from_CBM_calibration/PT_1995_MAKER_Const_FM_AR_VI_300.mdb.
```

```
##     3786 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/RO/from_CBM_calibration/RO_2010_MAKER_Const_Vb.mdb.
```

```
##     10428 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/SE/from_CBM_calibration/SW_1996_Const.mdb.
```

```
##     3888 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/SI/from_CBM_calibration/SL_2000_Const_V.mdb.
```

```
##     2897 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/SK/from_CBM_calibration/SK_2010_MAKER_Const_2.mdb.
```

```
##     5143 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/UK/from_CBM_calibration/UK_1997_Const_IV.mdb.
```

```
##     4807 rows, 39 columns
```

```r
disteventsnested %>% 
    checkcolumns() %>% 
    knitr::kable(format = "markdown")
```



|countryiso2                                                                    | ncol|colnames                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|:------------------------------------------------------------------------------|----:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|DK, FR                                                                         |   39|_1, _2, _3, _4, _5, _6, _7, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_type, Measurement_type, Amount, Dist_Type_ID, Step     |
|UK                                                                             |   39|_1, _2, _3, _4, _5, _6, _7, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_Type, Measurement_type, Amount, Dist_type_ID, Step     |
|AT, CZ, DE, EE, ES, FI, GR, HR, IE, IT, LT, LU, LV, NL, PL, PT, RO, SE, SI, SK |   39|_1, _2, _3, _4, _5, _6, _7, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_Type, Measurement_type, Amount, Dist_Type_ID, Step     |
|BE                                                                             |   39|_1, _2, _3, _4, _5, _6, _7, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_type, Mes_Type, Amount, Dist_Type_ID, Step             |
|HU                                                                             |   39|1, _2, _3, _4, _5, _6, _7, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_Type, Measurement_type, Tot_Amount, Dist_Type_ID, Step  |
|BG                                                                             |   40|_1, _2, _3, _4, _5, _6, _7, _8, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_Type, Measurement_type, Amount, Dist_Type_ID, Step |



