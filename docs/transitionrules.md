---
title: "Transition Rules"
author: "Paul"
date: "14 November 2018"
output: 
  html_document: 
    keep_md: yes
---





# Introduction

This document analyses how transition rules are modified by the CBM wrapper script in the 
[cbmwrap repository]()
The purpose is to create a function which will modify those rules. 


# Where are transition rules used ?

In the cbmwrap directory
```
$ grep -n Transition.csv *.py
cbm_update.py:110:        transPath=os.path.join(project_path, "Transition.csv")
cbm_update.py:111:        histTransPath=os.path.join(project_path, "Hist_Transition.csv")
upd_aws.py:48:        transPath=os.path.join(project_path, "Transition.csv")
upd_aws.py:49:        histTransPath=os.path.join(project_path, "Hist_Transition.csv")

$ grep -n Transition.csv *.R
expandFaws.R:30:transPath <- paste0(project_path, "/Transition.csv")
```

See also, still in the cbmwrap repository
```
$ grep transition *.py
cbm_mws.py:            sitDistTransitions, # name of disturbance transitions table
cbm_run.py:            sitDistTransitions, # name of disturbance transitions table
cbm_update.py:            sitDistTransitions, # name of disturbance transitions table
```

These transition rules are used by the  `runPysit` function in the
3 python scripts cb_update, cbm_mws and cbm_run:
```
$ grep -n sitDistTransitions *.py
cbm_mws.py:32:    def runPysit(self, pysitpath, pysitScriptPath, archiveIndexPath, sitInputDbPath, projectOutputDbPath, sitDistEventsTableN
ame, sitDistTransitions, sitYieldTables, runLength):
cbm_mws.py:42:            sitDistTransitions, # name of disturbance transitions table
cbm_run.py:20:    def runPysit(self, pysitpath, pysitScriptPath, archiveIndexPath, sitInputDbPath, projectOutputDbPath, sitDistEventsTableN
ame, sitDistTransitions, sitYieldTables, runLength):
cbm_run.py:30:            sitDistTransitions, # name of disturbance transitions table
cbm_update.py:34:    def runPysit(self, pysitpath, pysitScriptPath, archiveIndexPath, sitInputDbPath, projectOutputDbPath, sitDistEventsTab
leName, sitDistTransitions, sitYieldTables, runLength):
cbm_update.py:43:            sitDistTransitions, # name of disturbance transitions table
```

The runPysit function is called twice in cbm_update, 3 times in cbm_mws and once in cbm_run. 
For the first run in cbm_update.py, the transition rules table is called `"HIST_TRANSITION_CBM"`.
For all other model runs, the transition rule table used is always called `"TRANSITION_CBM"`.
```
$ grep -n -B 4 -A 8 "self.runPysit(" *.py
cbm_mws.py-445-        # adds the dummy disturbance at the end of the timestep
cbm_mws.py-446-
cbm_mws.py-447-
cbm_mws.py-448-        # call the pysit script for the no management run
cbm_mws.py:449:        self.runPysit(pysitpath,
cbm_mws.py-450-                 pysitScriptPath,
cbm_mws.py-451-                 archiveIndexPathWorking,
cbm_mws.py-452-                 sitPathCopied,
cbm_mws.py-453-                 workingProjectPath,
cbm_mws.py-454-                 "DIST_EVENTS_I",
cbm_mws.py-455-                 "TRANSITION_CBM",
cbm_mws.py-456-                 "YTS_CURRENT_SORTED",
cbm_mws.py-457-                 runLengthNew)
--
cbm_mws.py-552-        # insert result from R script to project db and generate new disturbance request table
cbm_mws.py-553-        self.mwsHarvestDemand(sitDb, R_outpath)
cbm_mws.py-554-
cbm_mws.py-555-        # call the pysit script for the MWS run
cbm_mws.py:556:        self.runPysit(pysitpath,
cbm_mws.py-557-                 pysitScriptPath,
cbm_mws.py-558-                 archiveIndexPathWorking,
cbm_mws.py-559-                 sitPathCopied,
cbm_mws.py-560-                 workingProjectPath,
cbm_mws.py-561-                 "DIST_EVENTS_II",
cbm_mws.py-562-                 "TRANSITION_CBM",
cbm_mws.py-563-                 "YTS_CURRENT_SORTED",
cbm_mws.py-564-                 runLengthNew)
--
cbm_mws.py-596-        # if necessary, loop to adjust harvest request
cbm_mws.py-597-        while os.path.exists(checkPath):
cbm_mws.py-598-
cbm_mws.py-599-            self.mwsHarvestDemand(sitDb, R_outpath)
cbm_mws.py:600:            self.runPysit(pysitpath,
cbm_mws.py-601-                     pysitScriptPath,
cbm_mws.py-602-                     archiveIndexPathWorking,
cbm_mws.py-603-                     sitPathCopied,
cbm_mws.py-604-                     workingProjectPath,
cbm_mws.py-605-                     "DIST_EVENTS_II",
cbm_mws.py-606-                     "TRANSITION_CBM",
cbm_mws.py-607-                     "YTS_CURRENT_SORTED",
cbm_mws.py-608-                     runLengthNew)
--
cbm_run.py-183-
cbm_run.py-184-        # import project db
cbm_run.py-185-        pysitpath = 'C:\\Dev\\Pysit\\1.2.6017.11\\pysit.exe'
cbm_run.py-186-        runLengthNew=int(endYear)-int(startYear) # calculate run length
cbm_run.py:187:        self.runPysit(pysitpath,
cbm_run.py-188-                 pysitScriptPath,
cbm_run.py-189-                 archiveIndexPathWorking,
cbm_run.py-190-                 sitPathCopied,
cbm_run.py-191-                 workingProjectPath,
cbm_run.py-192-                 "HARVEST_DEMAND",
cbm_run.py-193-                 "TRANSITION_CBM",
cbm_run.py-194-                 "YTS_CURRENT_SORTED",
cbm_run.py-195-                 runLengthNew)
--
cbm_update.py-414-        # launch R script to prepare ImportSIT.py
cbm_update.py-415-        self.callRScript(R_Executable_Path, R_SIT_path, R_script_args=[assPath,pysitScriptPath,toolboxPath, assXlPath]
)
cbm_update.py-416-
cbm_update.py-417-        # import project with historical tables
cbm_update.py:418:        self.runPysit(pysitpath,
cbm_update.py-419-             pysitScriptPath,
cbm_update.py-420-             archiveIndexPathWorking,
cbm_update.py-421-             sitPathCopied,
cbm_update.py-422-             workingProjectPath,
cbm_update.py-423-             "HIST_DIST_EVENTS_CBM",
cbm_update.py-424-             "HIST_TRANSITION_CBM",
cbm_update.py-425-             "YTS_HISTORICAL_SORTED",
cbm_update.py-426-             runLength)
--
cbm_update.py-577-                sitDb.ExecuteQuery(insertQuery, params=line)
cbm_update.py-578-       '''
cbm_update.py-579-
cbm_update.py-580-        # call the pysit script for DIST_EVENTS_Hist
cbm_update.py:581:        self.runPysit(pysitpath,
cbm_update.py-582-             pysitScriptPath,
cbm_update.py-583-             archiveIndexPathWorking,
cbm_update.py-584-             sitPathCopied,
cbm_update.py-585-             workingProjectPath,
cbm_update.py-586-             "Dist_Events_Const_Cal",
cbm_update.py-587-             "TRANSITION_CBM",
cbm_update.py-588-             "YTS_CURRENT_SORTED",
cbm_update.py-589-             runLength)
```




# Where are transition rules generated?

## `"HIST_TRANSITION_CBM"`

 * upd_aws.py contains a command `self.cursorToCsv(histTransPath, robDb.Query("Select * from HIST_TRANSITION_CBM"))` which copies the `HIST_TRANSITION_CBM` table from the
calibration database to a csv file `histTransPath`.
 * cbm_update.py contains a command `Insert into HIST_TRANSITION_CBM` 
with data coming from `histTransPath`.
```
$ grep -n histTransPath *.py
cbm_update.py:111:        histTransPath=os.path.join(project_path, "Hist_Transition.csv")
cbm_update.py:361:        with open(histTransPath, "rb") as f: # open the CSV file
upd_aws.py:49:        histTransPath=os.path.join(project_path, "Hist_Transition.csv")
upd_aws.py:71:        self.cursorToCsv(histTransPath, robDb.Query("Select * from HIST_TRANSITION_CBM"))
```

The `Hist_Transition.csv` file is used as is by `cbm_update.py` and is not modified by an R script
```
$ grep -n Hist_Transition.csv *.py
cbm_update.py:111:        histTransPath=os.path.join(project_path, "Hist_Transition.csv")
upd_aws.py:49:        histTransPath=os.path.join(project_path, "Hist_Transition.csv")

$ grep -n Hist_Transition.csv *.R
<no output>
```

`Hist_Transition.csv` is empty


```r
read.csv(file.path(datapath, "CZ", "Hist_Transition.csv")) %>% 
    head()
```

```
##  [1] X_1          X_2          X_3          X_4          X_5         
##  [6] X_6          X_7          UsingID      SWStart      SWEnd       
## [11] HWStart      HWEnd        Dist_Type_ID X__1         X__2        
## [16] X__3         X__4         X__5         X__6         X__7        
## [21] RegenDelay   ResetAge     Percent     
## <0 rows> (or 0-length row.names)
```


## `"TRANSITION_CBM"`


* upd_aws.py contains a command `self.cursorToCsv(transPath, robDb.Query("Select * from TRANSITION_CBM"))` which copies the `TRANSITION_CBM` table from the calibration database to a csv files `transPath`
* cbm_update.py contains a command `Insert into TRANSITION_CBM` with data coming from `transPath`.

```
$ grep -n transPath *
cbm_update.py:110:        transPath=os.path.join(project_path, "Transition.csv")
cbm_update.py:348:        with open(transPath, "rb") as f: # open the CSV file
/Rtools/bin/grep: data: Is a directory
/Rtools/bin/grep: docs: Is a directory
expandFaws.R:30:transPath <- paste0(project_path, "/Transition.csv")
expandFaws.R:217:transChar<-readLines(transPath)
expandFaws.R:222:writeLines(c(transHeader,transBody), con = transPath)
expandFaws.R:225:trans<-read.csv(transPath,stringsAsFactors = F)
expandFaws.R:231:    writeLines(c(transHeader,transBody,transBody2,transBody3), con = transPath)
expandFaws.R:235:trans <- read.csv(transPath, stringsAsFactors = F)
expandFaws.R:239:write.csv(trans, transPath, quote=F, row.names=F)
expandFaws.R:240:transBody<-readLines(transPath)[-1]
expandFaws.R:241:writeLines(c(transHeader,transBody), transPath)
Binary file upd_AWS.pyc matches
upd_aws.py:48:        transPath=os.path.join(project_path, "Transition.csv")
upd_aws.py:70:        self.cursorToCsv(transPath, robDb.Query("Select * from TRANSITION_CBM"))
```
`transPath` corresponds to the file `Transition.csv`, it is modified by `expandFaws.R`. 


Content of `Transition.csv`

```r
read.csv(file.path(datapath, "CZ", "Transition.csv")) %>% 
    head() %>% 
    knitr::kable(format="markdown")
```



|X_1 |X_2 |X_3 |X_4 |X_5 |X_6 |X_7   |UsingID | SWStart| SWEnd| HWStart| HWEnd| Dist_Type_ID|X__1 |X__2 |X__3 |X__4 |X__5 |X__6 |X__7  | RegenDelay| ResetAge| Percent|
|:---|:---|:---|:---|:---|:---|:-----|:-------|-------:|-----:|-------:|-----:|------------:|:----|:----|:----|:----|:----|:----|:-----|----------:|--------:|-------:|
|CC  |AA  |?   |H   |E   |?   |Con   |FALSE   |     100|   210|     100|   210|           18|CC   |AA   |?    |H    |E    |?    |Con   |          0|        1|     100|
|CC  |FS  |?   |H   |E   |?   |Broad |FALSE   |     110|   210|     110|   210|           16|CC   |FS   |?    |H    |E    |?    |Broad |          0|        1|     100|
|CC  |LD  |?   |H   |E   |?   |Con   |FALSE   |      90|   210|      90|   210|           18|CC   |LD   |?    |H    |E    |?    |Con   |          0|        1|     100|
|CC  |OB  |?   |H   |E   |?   |Broad |FALSE   |      60|   210|      60|   210|           16|CC   |OB   |?    |H    |E    |?    |Broad |          0|        1|     100|
|CC  |OC  |?   |H   |E   |?   |Con   |FALSE   |      40|   210|      40|   210|           18|CC   |OC   |?    |H    |E    |?    |Con   |          0|        1|     100|
|CC  |PA  |?   |H   |E   |?   |Con   |FALSE   |      90|   210|      90|   210|           18|CC   |PA   |?    |H    |E    |?    |Con   |          0|        1|     100|

