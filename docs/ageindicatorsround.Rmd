---
title: "ageindicatorsround"
author: "Paul"
date: "14 January 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
datapath <- "B:/BIOMASS/forbiomod1/EFDM/CBM"
```


# Which countries have the table `AgeIndicators_Round` ?

Find all table names in all country databases, then check which have the table `AgeIndicators_Round`.

```{r}




cbmcalibrationdbnested <- cbmio::findfilepath4all(datapath, subfolder = "from_CBM_calibration",
                                            pattern = ".*Const.*mdb$", colname = "calibdb") %>% 
    mutate(tables = map(calibdb, dbtables))

cbmcalibrationdb <- unnest(cbmcalibrationdbnested)



# Check countries which have a `AgeIndicators_Round` table.

# All remaining countries don't have that table


```

