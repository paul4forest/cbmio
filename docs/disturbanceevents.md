---
title: "Disturbance Events"
author: "Paul"
date: "August 8, 2018"
output:
  html_document:
    keep_md: yes
    toc: yes
---



# Introduction 
This document reads disturbance tables from the CBM databases. 

One section also deals with the development of a function that 
Prepares disturbance types with larger age ranges matching those of the silviculture table
See [BIOECONOMY-85](https://webgate.ec.europa.eu/CITnet/jira/browse/BIOECONOMY-85)


# Disturbance Types
## Read disturbance types from the CBM calibration database


```r
disttypesnested <- readcbmnested("Disturbance Types_CBM", datapath,
                                 subfolder = "from_CBM_calibration")
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/AT/from_CBM_calibration/AT_1998_Const_MAKER_IV_2c.mdb.
```

```
##     22 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/BE/from_CBM_calibration/BE_1999_MAKER_Const_4.mdb.
```

```
##     21 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/BG/from_CBM_calibration/BG_2010_MAKER_Const_6.mdb.
```

```
##     26 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/CZ/from_CBM_calibration/CZ_2000_MAKER_Const_IVb.mdb.
```

```
##     19 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/DE/from_CBM_calibration/DE_1992_Const_VI.mdb.
```

```
##     23 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/DK/from_CBM_calibration/DK_1994_MAKER_Const_IV.mdb.
```

```
##     21 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/EE/from_CBM_calibration/EST_2010_MAKER_Const_V.mdb.
```

```
##     22 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/ES/from_CBM_calibration/ES_1992_Const_VII.mdb.
```

```
##     21 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/FI/from_CBM_calibration/FI_1999_MAKER_Const_V.mdb.
```

```
##     19 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/FR/from_CBM_calibration/FR_1998_Const_VII.mdb.
```

```
##     29 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/GR/from_CBM_calibration/GR_1992_MAKER_Const_IVb.mdb.
```

```
##     25 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/HR/from_CBM_calibration/HR_1995_MAKER_Const_IVb.mdb.
```

```
##     27 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/HU/from_CBM_calibration/HU_2015_MAKER_Const_VI.mdb.
```

```
##     21 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/IE/from_CBM_calibration/IE_2005_FMAR_Const_VIII.mdb.
```

```
##     21 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/IT/from_CBM_calibration/IT_1995_Const_IV.mdb.
```

```
##     20 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/LT/from_CBM_calibration/LIT_1996_Const_IV.mdb.
```

```
##     27 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/LU/from_CBM_calibration/LUX_1999_Const_V.mdb.
```

```
##     25 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/LV/from_CBM_calibration/LV_2000_Const_IV.mdb.
```

```
##     22 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/NL/from_CBM_calibration/NL_1997_MAKER_Const_V.mdb.
```

```
##     19 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/PL/from_CBM_calibration/PL_2010_Const_4e.mdb.
```

```
##     21 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/PT/from_CBM_calibration/PT_1995_MAKER_Const_FM_AR_VI_300.mdb.
```

```
##     28 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/RO/from_CBM_calibration/RO_2010_MAKER_Const_Vb.mdb.
```

```
##     25 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/SE/from_CBM_calibration/SW_1996_Const.mdb.
```

```
##     21 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/SI/from_CBM_calibration/SL_2000_Const_V.mdb.
```

```
##     23 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/SK/from_CBM_calibration/SK_2010_MAKER_Const_2.mdb.
```

```
##     23 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/UK/from_CBM_calibration/UK_1997_Const_IV.mdb.
```

```
##     25 rows, 2 columns
```

```
## Loading Disturbance Types_CBM from C:/CBM/data/ZZ/from_CBM_calibration/CZ_2000_MAKER_Const_IVb.mdb.
```

```
##     19 rows, 2 columns
```

```r
disttypesnested %>% 
    checkcolumns() %>% 
    knitr::kable(format = "markdown")
```



|countryiso2                                                                                                | ncol|colnames                |
|:----------------------------------------------------------------------------------------------------------|----:|:-----------------------|
|AT, BE, BG, CZ, DE, DK, EE, ES, FI, FR, GR, HR, HU, IE, IT, LT, LU, LV, NL, PL, PT, RO, SE, SI, SK, UK, ZZ |    2|DisturbanceTypeID, Name |

```r
disttypesnested %>% 
    checkcolumntypes() %>% 
    knitr::kable(format = "markdown")
```



|countryiso2                                    |type                                            |
|:----------------------------------------------|:-----------------------------------------------|
|BE, CZ, DK, ES, FI, FR, HU, IE, IT, NL, SE, ZZ |DisturbanceTypeID : character, Name : character |
|AT, BG, DE, EE, GR, HR, LU, PL, SI, SK, UK     |DisturbanceTypeID : integer, Name : character   |
|LT, LV, PT, RO                                 |DisturbanceTypeID : numeric, Name : character   |

```r
# gsub(", ", '", "','AT, DE, EE, EL, GR, HR, LU, PL, SI, SK, UK') %>% cat()   
message("Countries for which DisturbanceTypeID is an integer variable")
```

```
## Countries for which DisturbanceTypeID is an integer variable
```

```r
disttypesinteger <- disttypesnested %>% 
    filter(countryiso2 %in% c("AT", "DE", "EE", "EL", "GR", "HR", "LU", "PL", "SI", "SK", "UK")) %>% 
    unnest() %>% ungroup()
disttypesinteger %>% 
    distinct(DisturbanceTypeID) %>% 
    unlist() %>% cat(sep = ", ")
```

```
## 7, 1, 9, 12, 14, 3, 16, 18, 8, 20, 2, 27, 11, 15, 23, 19, 13, 17, 21, 4, 22, 5, 6, 25, 26, 266, 10, 28, 113, 114
```

```r
message("Countries for which DisturbanceTypeID is a numeric variable")
```

```
## Countries for which DisturbanceTypeID is a numeric variable
```

```r
disttypesnumeric <- disttypesnested %>% 
    filter(countryiso2 %in% c("LT", "LV", "PT")) %>% 
    unnest() %>% ungroup()
disttypesnumeric %>% 
    distinct(DisturbanceTypeID) %>% 
    unlist() %>% cat(sep = ", ")
```

```
## 166, 1, 11, 12, 13, 14, 15, 16, 19, 2, 20, 21, 22, 23, 25, 26, 3, 4, 5, 6, 7, 8, 9, 27, 18, 111, 155, 10, 17, 28, 145, 29
```

```r
message("Countries for which DisturbanceTypeID is a character variable")
```

```
## Countries for which DisturbanceTypeID is a character variable
```

```r
disttypescharacter <- disttypesnested %>% 
    filter(countryiso2 %in% c("BE", "CZ", "DK", "ES", "FI", "HU", "IE", "IT", "NL", "SE")) %>% 
    # filter(countryiso2 %in% c("FR")) %>% 
    unnest() %>% ungroup() 

disttypescharacter %>% 
    distinct(DisturbanceTypeID) %>% 
    unlist() %>% cat(sep = ", ")
```

```
## DISTID1, DISTID11, DISTID2, DISTID3c, DISTID4c, DISTID5, DISTID6c, DISTID7c, DISTID8, DISTID9c, DISTID12, DISTID13, DISTID3b, DISTID6b, DISTID9_H, DISTID9b, DISTID4b, DISTID7b, DISTID9b_H, DISTID10, DISTID9c_H, DISTID7, DISTID16b, DISTID16c, DISTID9
```

## List disturbance types common to all countries


```r
# Change column type to character inside all the nested data frame
disttypes <- disttypesnested %>% 
    mutate(data = map(data, ~ 
                          .x %>% 
                          mutate(DisturbanceTypeID = as.character(DisturbanceTypeID)))) %>% 
    # checkcolumntypes()
    unnest() %>% 
    select(countryiso2, DisturbanceTypeID, Name)

# Write to csv files for printing
disttypescommon <- disttypes %>% 
    group_by(DisturbanceTypeID, Name) %>% 
    summarise(countryiso2 = paste(countryiso2, collapse = ", ")) %>% 
    ungroup() %>%
    # Natural sort
    slice(gtools::mixedorder(DisturbanceTypeID))

disttypescommon %>% 
    write.csv("B:/BIOMASS/CBM/docs/DisturbanceType.csv", row.names = FALSE)

# arrange id naturally
CNames = c("A1", "A10", "A11", "A12", "A2", "A3", "A4")
mixedsort(CNames)
```

```
## [1] "A1"  "A2"  "A3"  "A4"  "A10" "A11" "A12"
```

```r
mixedorder(CNames)
```

```
## [1] 1 5 6 7 2 3 4
```

Use clustering to group the names?
[Document clustering](https://cran.r-project.org/web/packages/textmineR/vignettes/b_document_clustering.html)

# Read disturbance events from the CBM calibration Databases 


```r
disteventsnested <-  readcbmnested("Dist_Events_Const", datapath,
                                   subfolder = "from_CBM_calibration")
```

```
## Loading Dist_Events_Const from C:/CBM/data/AT/from_CBM_calibration/AT_1998_Const_MAKER_IV_2c.mdb.
```

```
##     10657 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/BE/from_CBM_calibration/BE_1999_MAKER_Const_4.mdb.
```

```
##     2550 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/BG/from_CBM_calibration/BG_2010_MAKER_Const_6.mdb.
```

```
##     5927 rows, 40 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/CZ/from_CBM_calibration/CZ_2000_MAKER_Const_IVb.mdb.
```

```
##     2912 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/DE/from_CBM_calibration/DE_1992_Const_VI.mdb.
```

```
##     13659 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/DK/from_CBM_calibration/DK_1994_MAKER_Const_IV.mdb.
```

```
##     2223 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/EE/from_CBM_calibration/EST_2010_MAKER_Const_V.mdb.
```

```
##     2333 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/ES/from_CBM_calibration/ES_1992_Const_VII.mdb.
```

```
##     5546 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/FI/from_CBM_calibration/FI_1999_MAKER_Const_V.mdb.
```

```
##     1442 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/FR/from_CBM_calibration/FR_1998_Const_VII.mdb.
```

```
##     19553 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/GR/from_CBM_calibration/GR_1992_MAKER_Const_IVb.mdb.
```

```
##     866 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/HR/from_CBM_calibration/HR_1995_MAKER_Const_IVb.mdb.
```

```
##     3751 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/HU/from_CBM_calibration/HU_2015_MAKER_Const_VI.mdb.
```

```
##     2128 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/IE/from_CBM_calibration/IE_2005_FMAR_Const_VIII.mdb.
```

```
##     7117 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/IT/from_CBM_calibration/IT_1995_Const_IV.mdb.
```

```
##     18023 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/LT/from_CBM_calibration/LIT_1996_Const_IV.mdb.
```

```
##     3666 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/LU/from_CBM_calibration/LUX_1999_Const_V.mdb.
```

```
##     1899 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/LV/from_CBM_calibration/LV_2000_Const_IV.mdb.
```

```
##     2491 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/NL/from_CBM_calibration/NL_1997_MAKER_Const_V.mdb.
```

```
##     2506 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/PL/from_CBM_calibration/PL_2010_Const_4e.mdb.
```

```
##     26840 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/PT/from_CBM_calibration/PT_1995_MAKER_Const_FM_AR_VI_300.mdb.
```

```
##     3786 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/RO/from_CBM_calibration/RO_2010_MAKER_Const_Vb.mdb.
```

```
##     10428 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/SE/from_CBM_calibration/SW_1996_Const.mdb.
```

```
##     3888 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/SI/from_CBM_calibration/SL_2000_Const_V.mdb.
```

```
##     2897 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/SK/from_CBM_calibration/SK_2010_MAKER_Const_2.mdb.
```

```
##     5143 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/UK/from_CBM_calibration/UK_1997_Const_IV.mdb.
```

```
##     4807 rows, 39 columns
```

```
## Loading Dist_Events_Const from C:/CBM/data/ZZ/from_CBM_calibration/CZ_2000_MAKER_Const_IVb.mdb.
```

```
##     2912 rows, 39 columns
```

```r
disteventsnested %>% 
    checkcolumns() %>% 
    knitr::kable(format = "markdown")
```



|countryiso2                                                                            | ncol|colnames                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|:--------------------------------------------------------------------------------------|----:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|BE, DK, FR                                                                             |   39|_1, _2, _3, _4, _5, _6, _7, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_type, Measurement_type, Amount, Dist_Type_ID, Step     |
|UK                                                                                     |   39|_1, _2, _3, _4, _5, _6, _7, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_Type, Measurement_type, Amount, Dist_type_ID, Step     |
|AT, CZ, DE, EE, ES, FI, GR, HR, HU, IE, IT, LT, LU, LV, NL, PL, PT, RO, SE, SI, SK, ZZ |   39|_1, _2, _3, _4, _5, _6, _7, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_Type, Measurement_type, Amount, Dist_Type_ID, Step     |
|BG                                                                                     |   40|_1, _2, _3, _4, _5, _6, _7, _8, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_Type, Measurement_type, Amount, Dist_Type_ID, Step |

```r
# A named vector to rename cbm columns
cbmcolumnrename <- c("1"            = "_1",
                     "Sort_type"    = "Sort_Type",
                     "Mes_Type"     = "Measurement_type",
                     "Tot_Amount"   = "Amount",
                     "Dist_type_ID" = "Dist_Type_ID") 
# Massage the data
distevents<- disteventsnested %>% 
    mutate(data = map(data, renamecolumnstemporarily, renamevect = cbmcolumnrename)) %>% 
    mutate(data = map(data, renameclassifierscolumns)) %>% 
    mutate(data = map(data, ~ 
                          .x %>% 
                          mutate(UsingID = as.logical(UsingID),
                                 Dist_Type_ID = as.character(Dist_Type_ID)))) %>% 
    unnest(data) %>% 
    ungroup() %>% 
    rename_all(tolower) %>% 
    select(-path, -nfile, -accessfilepath)
```

Display unique values in all columns of the disturbance events table, except the "amount" column.

```r
distevents %>% 
    select(-amount) %>% 
    lapply(unique)
```

```
## $countryiso2
##  [1] "AT" "BE" "BG" "CZ" "DE" "DK" "EE" "ES" "FI" "FR" "GR" "HR" "HU" "IE"
## [15] "IT" "LT" "LU" "LV" "NL" "PL" "PT" "RO" "SE" "SI" "SK" "UK" "ZZ"
## 
## $cl1
## [1] "For" "?"   "AR"  "NF"  "CC"  "Th"  "Un" 
## 
## $cl2
##  [1] "AA"        "FS"        "LD"        "OC"        "OH"       
##  [6] "PA"        "PC"        "PN"        "PS"        "QR"       
## [11] "OS"        "BT"        "DF"        "OB"        "PT"       
## [16] "FO"        "QC"        "RF"        "OL"        "AG"       
## [21] "AI"        "BA"        "BM"        "CA"        "CM"       
## [26] "PP"        "?"         "PP_NF"     "OB_NF"     "PS_NF"    
## [31] "EG_NF"     "CB"        "CS"        "PM"        "QI"       
## [36] "QP"        "NF_RP"     "NF_QR"     "NF_PS"     "NF_PA"    
## [41] "NF_FS"     "NF_OH"     "NF_OS"     "RP"        "PA_NF"    
## [46] "OE"        "Oca"       "QS"        "BP"        "CP"       
## [51] "EG"        "QH"        "ConBroad"  "PredBroad" "PredCon"  
## [56] "BT_NF"     "FS_NF"     "OC_NF"     "QR_NF"    
## 
## $cl3
##   [1] "?"      "AT11"   "AT12"   "AT31"   "AT34"   "AT21"   "AT22"  
##   [8] "AT32"   "AT33"   "BG00"   "DE_Hes" "DE_Sar" "DE_Thu" "DE_M-V"
##  [15] "DE_Sac" "DE_S-H" "DE_N-W" "DE_NHB" "DE_B-W" "DE_R-P" "DE_B-B"
##  [22] "DE_S-A" "DE_Bay" "EE00"   "ES62"   "ES70"   "ES11"   "ES12"  
##  [29] "ES13"   "ES21"   "ES22"   "ES23"   "ES24"   "ES30"   "ES52"  
##  [36] "ES51"   "ES61"   "ES43"   "ES41"   "ES42"   "ES53"   "FI00"  
##  [43] "FR24"   "FR52"   "FR41"   "FR53"   "FR30"   "FR42"   "FR62"  
##  [50] "FR21"   "FR63"   "FR51"   "FR43"   "FR10"   "FR72"   "FR71"  
##  [57] "FR22"   "FR26"   "FR23"   "FR61"   "FR81"   "FR25"   "FR82"  
##  [64] "FR83"   "HR00"   "HU00"   "IE00"   "Cal"    "Cam"    "ER"    
##  [71] "FVG"    "La"     "Lo"     "Mo"     "Pi"     "Pu"     "Sa"    
##  [78] "Si"     "Um"     "Ve"     "AA"     "Ab"     "Li"     "Tn"    
##  [85] "To"     "Ma"     "Va"     "Ba"     "LT00"   "LU00"   "LV00"  
##  [92] "NL00"   "PL00"   "RO22"   "RO31"   "RO41"   "RO42"   "RO12"  
##  [99] "RO21"   "RO11"   "SE21"   "SE22"   "SE23"   "SE11"   "SE12"  
## [106] "SE31"   "SE32"   "SE33"   "SL00"   "SK00"   "UKE0"   "UKM0"  
## [113] "UKL0"   "UKN0"  
## 
## $cl4
##  [1] "H"  "C"  "?"  "R"  "M"  "E"  "A"  "B"  "D"  "S"  "BT" "CP" "MP" "MS"
## [15] "MZ" "SD" "SL" "WP" "P" 
## 
## $cl5
## [1] "P" "S" "?" "E" "T" "U" "1" "2"
## 
## $cl6
##  [1] "?"   "35"  "45"  "55"  "65"  "25"  "340" "350" "34"  "250" "44" 
## [12] "54"  "46"  "64"  "74"  "75"  "85" 
## 
## $cl7
## [1] "Con"   "Broad" "?"    
## 
## $usingid
## [1] FALSE
## 
## $swstart
##   [1]  10  40 110  70 130  20  30  22 120  95  35   8  11  12  13  14  16
##  [18]  17  18  19  23  24  25  26  28  29  31  32  34  36  37  38  41  42
##  [35]  43  44  46  47  48  49  50  52  53  54  55  56  58  59  60  61  62
##  [52]  64  65  66  67  68  71  72  73  74  76  77  78  79  80  82  83  84
##  [69]  85  86  88  89  90  91  92  94  96  97  98 100 101 102 103 104 106
##  [86] 107 108 109 112 113 114 115 116 118 119   2   3   4   5   6   7   9
## [103]  15  21  27  33  39  45  51  57  63  69  75  81  87  99 140   1   0
## [120] 150 105
## 
## $swend
##   [1]  40 110 210  70 120 105  30 115  35  75  65  22  90  80 251 250 248
##  [18] 247 246 244 243 242 240 239 238 237 235 234 233 231 230 229 228 226
##  [35] 225 224 222 221 220 218 217 216 214 213 212 211 209 208 207 205 204
##  [52] 203 202 200 199 198 196 195 194 192 191 190 188 187 186 185 183 182
##  [69] 181 179 178 177 176 174 173 172 170 169 168 166 165 164 162 161 160
##  [86] 159 157 156 155 153 152 151 150 148 147 146 144 143 142 140 139 138
## [103] 136 135 134 133 131 130  77  76  74  73  72  71  69  68  66  67  64
## [120]  62  63  61  60  58  59  57  56  54  55  53  52  50  51  49  48  46
## [137]  47  45  44  42  43  41 154 149 145 141 137 132 129 128 126 127 125
## [154] 123 124 122 121 118 119 116 117 114 113 112 111 109 107 108 106 104
## [171] 102 103 100 101  99  98  97  96  94  95  93  91  92  89  88  86  87
## [188]  84  85  83  82  81  78  79 171 167 163 158 232 227 223 215 206 197
## [205] 193 184 180 175  25  26  36  29 260 280  28  31  20  21  23  24  27
## [222] 189 201
## 
## $hwstart
##   [1]  10  40 110  70 130  20  30  22 120  95  35   8  11  12  13  14  16
##  [18]  17  18  19  23  24  25  26  28  29  31  32  34  36  37  38  41  42
##  [35]  43  44  46  47  48  49  50  52  53  54  55  56  58  59  60  61  62
##  [52]  64  65  66  67  68  71  72  73  74  76  77  78  79  80  82  83  84
##  [69]  85  86  88  89  90  91  92  94  96  97  98 100 101 102 103 104 106
##  [86] 107 108 109 112 113 114 115 116 118 119   2   3   4   5   6   7   9
## [103]  15  21  27  33  39  45  51  57  63  69  75  81  87  99 140   1   0
## [120] 150 105
## 
## $hwend
##   [1]  40 110 210  70 120 105  30 115  35  75  65  22  90  80 251 250 248
##  [18] 247 246 244 243 242 240 239 238 237 235 234 233 231 230 229 228 226
##  [35] 225 224 222 221 220 218 217 216 214 213 212 211 209 208 207 205 204
##  [52] 203 202 200 199 198 196 195 194 192 191 190 188 187 186 185 183 182
##  [69] 181 179 178 177 176 174 173 172 170 169 168 166 165 164 162 161 160
##  [86] 159 157 156 155 153 152 151 150 148 147 146 144 143 142 140 139 138
## [103] 136 135 134 133 131 130  77  76  74  73  72  71  69  68  66  67  64
## [120]  62  63  61  60  58  59  57  56  54  55  53  52  50  51  49  48  46
## [137]  47  45  44  42  43  41 154 149 145 141 137 132 129 128 126 127 125
## [154] 123 124 122 121 118 119 116 117 114 113 112 111 109 107 108 106 104
## [171] 102 103 100 101  99  98  97  96  94  95  93  91  92  89  88  86  87
## [188]  84  85  83  82  81  78  79 171 167 163 158 232 227 223 215 206 197
## [205] 193 184 180 175  25  26  36  29 260 280  28  31  20  21  23  24  27
## [222] 189 201
## 
## $min_since_last_dist
##  [1] -1  7 10  8 20 12 15  1  5  9  6 13  4 21 16 22 18 19 30 14  3
## 
## $max_since_last_dist
## [1] -1  3
## 
## $last_dist_id
## [1] -1 27  5 17 18  4
## 
## $min_tot_biom_c
## [1] -1
## 
## $max_tot_biom_c
## [1] -1
## 
## $min_merch_soft_biom_c
## [1] -1
## 
## $max_merch_soft_biom_c
## [1] -1
## 
## $min_merch_hard_biom_c
## [1] -1
## 
## $max_merch_hard_biom_c
## [1] -1
## 
## $min_tot_stem_snag_c
## [1] -1
## 
## $max_tot_stem_snag_c
## [1] -1
## 
## $min_tot_soft_stem_snag_c
## [1] -1
## 
## $max_tot_soft_stem_snag_c
## [1] -1
## 
## $min_tot_hard_stem_snag_c
## [1] -1
## 
## $max_tot_hard_stem_snag_c
## [1] -1
## 
## $min_tot_merch_stem_snag_c
## [1] -1
## 
## $max_tot_merch_stem_snag_c
## [1] -1
## 
## $min_tot_merch_soft_stem_snag_c
## [1] -1
## 
## $max_tot_merch_soft_stem_snag_c
## [1] -1
## 
## $min_tot_merch_hard_stem_snag_c
## [1] -1
## 
## $max_tot_merch_hard_stem_snag_c
## [1] -1
## 
## $efficency
## [1] 1
## 
## $sort_type
## [1]  3 12  6  2  1
## 
## $measurement_type
## [1] "M" "A" "P"
## 
## $dist_type_id
##  [1] "12"         "14"         "16"         "11"         "13"        
##  [6] "15"         "21"         "22"         "27"         "4"         
## [11] "5"          "9"          "DISTID4b"   "DISTID9b"   "DISTID4c"  
## [16] "DISTID6c"   "DISTID9c"   "DISTID6b"   "DISTID5"    "26"        
## [21] "6"          "DISTID10"   "DISTID9b_H" "DISTID9c_H" "19"        
## [26] "20"         "DISTID16b"  "DISTID16c"  "DISTID8"    "DISTID1"   
## [31] "DISTID3b"   "DISTID3c"   "31"         "17"         "32"        
## [36] "30"         "1"          "33"         "3"          "113"       
## [41] "114"        "DISTID12"   "155"        "166"        "23"        
## [46] "7"          "18"         "29"         "145"        "8"         
## 
## $step
##   [1]   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17
##  [18]  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34
##  [35]  35  36  37  38  39  40  41  42  43  44  45  46  47  48  49  50  51
##  [52]  52  53  54  55  56  57  58  59  60  61  62  63  64  65  66  67  68
##  [69]  69  70  71  72  73  74  75  76  77  78  79  80  81  82  83  84  85
##  [86]  86  87  88  89  90  91  92  93  94  95  96  97  98  99 100 101 102
## [103] 103 104 105 106 107 108 109 110 111
## 
## $cl8
##  [1] NA   "25" "29" "28" "8"  "14" "42" "21" "38" "22" "34" "5"  "4"  "7" 
## [15] "18" "19" "20" "40" "15" "9"  "32" "23" "33" "13" "16" "37" "35" "43"
## [29] "2"  "36" "12" "17" "41" "31" "10" "11" "3"  "30" "1"  "39" "?"
```




## Classifier 1 and step

```r
distevents %>% 
    group_by(countryiso2, cl1) %>% 
    summarise(minstep = min(step),
              maxstep = max(step))
```

```
## # A tibble: 49 x 4
## # Groups:   countryiso2 [?]
##    countryiso2 cl1   minstep maxstep
##    <chr>       <chr>   <dbl>   <dbl>
##  1 AT          For         1     103
##  2 BE          For         1     102
##  3 BG          For         1     101
##  4 CZ          For         1     101
##  5 DE          For         1     109
##  6 DK          For         1     107
##  7 EE          For         1     101
##  8 ES          ?           1     109
##  9 ES          AR          2     109
## 10 ES          For         1     109
## # ... with 39 more rows
```


```r
distevents %>% 
    group_by(countryiso2, step, cl1) %>% 
    tally() %>% 
    ggplot(aes(x = step, y = n)) + 
    geom_point() +
    facet_grid(countryiso2 ~ cl1, scales = "free_y") +
    ylab("Number of lines in the 'Dist_Events_Const' Access table") +
    theme_bw()
```

![](disturbanceevents_files/figure-html/plotcl1andstep-1.png)<!-- -->

```r
if(FALSE){ # Save plot only explicitly
    ggsave("C:/Users/rougipa/Downloads/disturbanceeventsclassifier1n.pdf", width = 12, height = 30) 
}    
```



## Area affected by disturbance for each category of classifier one

```r
distevents %>% 
    ggplot(aes(x = step, y = amount)) +
    geom_bar(stat="identity", fill="grey") +
    facet_grid(countryiso2 ~ cl1, scales = "free_y") +
    theme_bw()
```

![](disturbanceevents_files/figure-html/plotdisturbanceeventsclassifier1amount-1.png)<!-- -->

```r
if(FALSE){ # Save plot only explicitly
    ggsave("C:/Users/rougipa/Downloads/disturbanceeventsclassifier1amount.pdf", width = 12, height = 30) 
}    
```


## Check the minus one columns -1

```r
distevensmone <- distevents %>% 
    select(countryiso2, min_since_last_dist:max_tot_merch_hard_stem_snag_c) %>% 
    group_by(countryiso2) %>% 
    gather(variable, value, -countryiso2) %>% 
    distinct() %>% 
    filter(value>-1) %>%
    group_by(countryiso2, variable) %>% 
    summarise(value = paste(value, collapse = "_")) %>% 
    spread(variable, value)

write.csv(distevensmone, "B:/BIOMASS/CBM/disteventsmone.csv")
```


# Read disturbance events from the CBM `SITDB_Working.mdb` databases


```r
disteventsnested <-  readcbmnested("Dist_Events_Const", datapath,
                                   subfolder = "from_CBM_calibration",
                                   pattern = "SITDB_Working.mdb")
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/AT.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/BE.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/BG.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/CZ.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/DE.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/DK.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/EE.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/ES.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/FI.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/FR.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/GR.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/HR.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/HU.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/IE.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/IT.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/LT.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/LU.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/LV.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/NL.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/PL.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/PT.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/RO.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/SE.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/SI.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/SK.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/UK.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Warning in .f(.x[[i]], ...): 
## There is an issue in C:/CBM/data/ZZ.
## Number of access files matching the pattern 'SITDB_Working.mdb': 0
```

```
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
## Loading Dist_Events_Const from NA.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code 63, message [Microsoft][ODBC Microsoft
## Access Driver]General error Unable to open registry key Temporary
## (volatile) Ace DSN for process 0x14ec Thread 0x1110 DBC 0x4580338 Jet'.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## [RODBC] ERROR: state HY000, code -1044, message [Microsoft][ODBC Microsoft
## Access Driver] Not a valid file name.
```

```
## Warning in RODBC::odbcDriverConnect(sprintf("Driver=%s;DBQ=%s", driver, :
## ODBC connection failed
```

```
##      rows,  columns
```



# Prepare disturbance types with larger age ranges matching those of the silviculture table

## Load data from the Czech Republic to experiment with 
Read the disturbance table coming from fusion into `distfusionCZ`
and read the original disturbance table calculatd from the calibration into `distcalCZ`. 

```r
countryiso2 <- "CZ"
distfusionCZ <- read.csv(file.path(datapath, countryiso2, "Disturbance2016_2020.csv"))
distcalCZ <- read.csv(file.path(datapath, countryiso2, "Dist_Events_Const_Cal.csv"))
disttypesCZ <- read.csv(file.path(datapath, countryiso2, "DisturbanceTypes.csv"))
silvicultureCZ <- read.csv(file.path(datapath, countryiso2, "silviculture.csv"))
```


## Plot disturbance age range by region and by species

Plot the age ranges for the disturbance table, coming from fusion, 
the original disturbance table from the calibration and the silvicultureCZ table.


```r
distfusionCZ %>% 
    filter(X_1 == "CC", X_2 == "OB", X_3 == "CZ05", X_6 == 25, X_7 == "Broad") %>% 
    ggplot(aes(x = STEP)) +
    geom_errorbar(aes(ymin = SWStart, ymax = SWEnd)) +
        facet_wrap(~dist_type_id)
```

![](disturbanceevents_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

```r
distcalCZ %>% 
    filter(X_2 == "OB", X_7 == "Broad") %>% 
    ggplot(aes(x = Step)) +
    geom_errorbar(aes(ymin = SWStart, ymax = SWEnd)) +
    facet_wrap(~Dist_Type_ID)
```

![](disturbanceevents_files/figure-html/unnamed-chunk-6-2.png)<!-- -->

```r
silvicultureCZ %>% 
    filter(X_2 == "OB") %>% 
    ggplot(aes(x = as.factor(Dist_Type_ID))) +
    geom_errorbar(aes(ymin = Min_age, ymax = Max_age)) +
    facet_wrap(~X_1)
```

![](disturbanceevents_files/figure-html/unnamed-chunk-6-3.png)<!-- -->
Same plot for all regions



```r
distfusionCZ %>% 
    filter(X_1 == "CC", X_2 == "OB",  X_6 == 25, X_7 == "Broad") %>% 
    ggplot(aes(x = STEP)) +
    geom_errorbar(aes(ymin = SWStart, ymax = SWEnd)) +
        facet_grid(dist_type_id ~ X_3) + 
    ggtitle("Age range by region and dist type id (filter: _1==CC & _2==OB)")
```

![](disturbanceevents_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

```r
if(FALSE){
    ggsave("B:/BIOMASS/CBM/plots/agerangeregion_3.pdf", width = 10, height = 10)
}
```

Same plot for all X_6


```r
distfusionCZ %>% 
    filter(X_1 == "CC", X_2 == "OB", X_3 == "CZ05", X_7 == "Broad") %>% 
    ggplot(aes(x = STEP)) +
    geom_errorbar(aes(ymin = SWStart, ymax = SWEnd)) +
        facet_wrap(X_6~dist_type_id) +
    ggtitle("Age range by classifier _6 and dist type id")
```

![](disturbanceevents_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

```r
if(FALSE){
    ggsave("B:/BIOMASS/CBM/plots/agerange_6.pdf", width = 10, height = 10)
}
```

Same plot for all species 

```r
distfusionCZ %>% 
    filter(X_1 == "CC",  X_3 == "CZ05", X_6 == 25, X_7 == "Broad") %>% 
    ggplot(aes(x = STEP)) +
    geom_errorbar(aes(ymin = SWStart, ymax = SWEnd)) +
    facet_grid(dist_type_id ~ X_2) + 
    ggtitle("Age range by species and dist type id (filter: _1==CC & _3==CZ05)")
```

![](disturbanceevents_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

```r
if(FALSE){
    ggsave("B:/BIOMASS/CBM/plots/agerangespecies_2.pdf", width = 10, height = 10)
}
```
Age ragne by species dist type id and region


```r
distfusionCZ %>% 
    filter(X_1 == "CC",  X_6 == 25, X_7 == "Broad") %>% 
    ggplot(aes(x = STEP)) +
    geom_errorbar(aes(ymin = SWStart, ymax = SWEnd)) +
    facet_grid(dist_type_id + X_2 ~ X_3) + 
    ggtitle("Age range by species and dist type id (filter: _1==CC )")
```

![](disturbanceevents_files/figure-html/unnamed-chunk-10-1.png)<!-- -->

```r
if(FALSE){
    ggsave("B:/BIOMASS/CBM/plots/agerangespecies_2region_3.pdf", width = 10, height = 10)
}
```


## Join the silvicultureCZ and the disturbance tables by the classifier column


### Join dist calibration

```r
dput(names(select(distcalCZ, contains("X_"))))
```

```
## c("X_1", "X_2", "X_3", "X_4", "X_5", "X_6", "X_7", "Max_since_last_Dist", 
## "Max_tot_biom_C", "Max_merch_soft_biom_C", "Max_merch_hard_biom_C", 
## "Max_tot_stem_snag_C", "Max_tot_soft_stem_snag_C", "Max_tot_hard_stem_snag_C", 
## "Max_tot_merch_stem_snag_C", "Max_tot_merch_soft_stem_snag_C", 
## "Max_tot_merch_hard_stem_snag_C")
```

```r
dput(names(select(silvicultureCZ, contains("X_"))))
```

```
## c("X_1", "X_2", "X_4", "X_5", "X_7", "Max_age", "Max_since_last"
## )
```

```r
# Do not join by colmun X_1 because it contains ? in distcalCZ
distcalCZjoined <- distcalCZ %>% 
    select(-X_1) %>% 
    left_join(silvicultureCZ, 
              by = c("X_2", "X_4", "X_5", "X_7", "Dist_Type_ID"))

distcalCZccjoined <- distcalCZjoined %>% filter(X_1 == "CC")


# Plot age range dist and age range cal on the same plot
distcalCZccjoined %>% 
    ggplot() +
    geom_errorbar(aes(ymin = SWStart, ymax = SWEnd, x = Step + 0.5, colour ="dist")) +
    geom_errorbar(aes(ymin = Min_age, ymax = Max_age, x = Step, colour = "silvicultureCZ")) +
    facet_wrap(~Dist_Type_ID)
```

![](disturbanceevents_files/figure-html/unnamed-chunk-11-1.png)<!-- -->

```r
message("Now add the silvicultureCZ data, in different colors")
```

```
## Now add the silvicultureCZ data, in different colors
```

### Join dist fusion

This time join by the `X_1` variable as well

```r
distfusionCZjoined <- distfusionCZ %>% 
    rename(Dist_Type_ID = dist_type_id) %>% 
    left_join(silvicultureCZ, 
              by = c("X_1", "X_2", "X_4", "X_5", "X_7", "Dist_Type_ID"))


# Plot age range dist and age range cal on the same plot
distfusionCZjoined %>% 
    ggplot() +
    geom_errorbar(aes(x = STEP+0.5, ymin = SWStart, ymax = SWEnd, colour = "dist fusion")) +
    geom_errorbar(aes(x = STEP, ymin = Min_age, ymax = Max_age, colour = "silvicultureCZ")) +
    facet_wrap(~Dist_Type_ID + X_1)
```

![](disturbanceevents_files/figure-html/unnamed-chunk-12-1.png)<!-- -->


Show the age range variables of interest

```r
# 7 classifiers and  age ranges only
clagerange <- distfusionCZjoined %>% 
    select(X_1,X_2,X_3,X_4,X_5,X_6,X_7,
           SWStart, SWEnd, Min_age, Max_age) 

# Most frequent age combination:
distfusionCZjoined %>% 
    group_by(SWStart, SWEnd, Min_age, Max_age) %>% 
    tally() %>% 
    arrange(desc(n)) %>% 
    head()
```

```
## # A tibble: 6 x 5
## # Groups:   SWStart, SWEnd, Min_age [6]
##   SWStart SWEnd Min_age Max_age     n
##     <int> <int>   <int>   <int> <int>
## 1      23    32      10      30    20
## 2      13    22      10      30    17
## 3      25    34      20      40    17
## 4      11    20      10      30    16
## 5      25    34      10      30    16
## 6     105   114      90     210    16
```

```r
# Prepare a sample dataset for testing purposes , see ./tests/testthat
# Chage factors to character variables
clagerange[sapply(clagerange, is.factor)] <- lapply(clagerange[sapply(clagerange, is.factor)], as.character)
dput(head(clagerange))
```

```
## structure(list(X_1 = c("CC", "CC", "CC", "CC", "CC", "CC"), X_2 = c("AA", 
## "AA", "AA", "AA", "AA", "AA"), X_3 = c("CZ02", "CZ02", "CZ02", 
## "CZ02", "CZ02", "CZ03"), X_4 = c("H", "H", "H", "H", "H", "H"
## ), X_5 = c("E", "E", "E", "E", "E", "E"), X_6 = c(25L, 25L, 25L, 
## 25L, 25L, 25L), X_7 = c("Con", "Con", "Con", "Con", "Con", "Con"
## ), SWStart = c(161L, 181L, 41L, 71L, 81L, 41L), SWEnd = c(170L, 
## 190L, 50L, 80L, 90L, 50L), Min_age = c(100L, 100L, 40L, 40L, 
## 40L, 40L), Max_age = c(210L, 210L, 100L, 100L, 100L, 100L)), row.names = c(NA, 
## 6L), class = "data.frame")
```

```r
# The idea was dropped for a simpler data set
# keeping this here for the record
dtf <-  data.frame(X_1 = c("CC", "CC", "CC", "CC", "CC", "CC"),
                  X_2 = c("AA",  "AA", "AA", "AA", "AA", "AA"),
                  X_3 = c("CZ02", "CZ02", "CZ02",  "CZ02", "CZ02", "CZ03"),
                  X_4 = c("H", "H", "H", "H", "H", "H" ),
                  X_5 = c("E", "E", "E", "E", "E", "E"),
                  X_6 = c(25L, 25L, 25L,  25L, 25L, 25L),
                  X_7 = c("Con", "Con", "Con", "Con", "Con", "Con" ),
                  SWStart = c(161L, 181L, 41L, 71L, 81L, 41L),
                  SWEnd = c(170L,  190L, 50L, 80L, 90L, 50L),
                  Min_age = c(100L, 100L, 40L, 40L,  40L, 40L),
                  Max_age = c(210L, 210L, 100L, 100L, 100L, 100L))

# Replace age range by those of the 
```



## Develop a function `copydistagerange()`
This function will have a csv file as an input and a csv file as an output. 
Will it overwrite the file? 
It shouldn't in my opinion. 

The grouping variables should be given as a parameter to the function, 
to allow some flexibility and to make test datasets smaller.
[group_by_all or group_by_at](https://dplyr.tidyverse.org/reference/group_by_all.html) 
may be used for that purpose?


```r
distfusionCZ  %>% 
    group_by_at(vars(starts_with("X_"))) %>% 
    tally() %>% 
    arrange(desc(n))

distfusionCZ  %>% select(starts_with("X_")) %>% names() %>% dput()

groupingvars <- c("X_1", "X_2", "X_3", "X_4", "X_5", "X_6", "X_7")
distfusionCZ %>% 
    group_by_at(groupingvars) %>% 
    tally() %>% 
    arrange(desc(n))
# Select only the variables of interest from the silvicultureCZ table
silvicultureCZ %>% 
    select(groupingvars, Min_age, Max_age)
```


## Use the function `copydistagerange()`

```r
silvicultureCZ <- silvicultureCZ %>% 
    rename(dist_type_id = Dist_Type_ID) # Get same variables name for silvicultureCZ and disturbance 
joinvars <- c("X_1", "X_2", "X_4", "X_5", "X_7", "dist_type_id")

distfusionCZ2 <- distfusionCZ %>% 
    copydistagerange(select(silvicultureCZ, -Efficency, -Sort_Type),
                     by = joinvars) %>% 
    sumdistamount()
```


## Prepare a porcelaine function for direct use with csv files as arguments

```r
# With default arguments
copydistagerangeandsumamount("C:/CBM/data", "CZ")

# Specifying all arguments
copydistagerangeandsumamount('C:/CBM/data', 'CZ', disturbanceinputcsv = 'Disturbance2016_2020.csv', silviculturecsv = 'silviculture.csv', disturbanceoutputcsv = 'Disturbance2016_2020sum.csv')
```

The porcelaine function can be called with Rscript (for lack of a better option)
One of the calls below will be placed in a cbmwrap python file. 
Probably the one that is using `disturbance2016_2020.csv` from fusion, that is cbm_run.py.
```
# With default values for the csv file name parameters
"C:\Program Files\R\R-3.5.0\bin\Rscript.exe" -e "library(cbmio); copydistagerangeandsumamount('C:/CBM/data', 'CZ')"

# Specifying the file name csv parameters
"C:\Program Files\R\R-3.5.0\bin\Rscript.exe" -e "library(cbmio); copydistagerangeandsumamount('C:/CBM/data', 'CZ', disturbanceinputcsv = 'Disturbance2016_2020.csv', silviculturecsv = 'silviculture.csv', disturbanceoutputcsv = 'Disturbance2016_2020sum.csv')"
```

Where can this updated disturbance file be created?

1. Either in the cbm_run.py script directly
2. Or in the markdown file that calls cbm_run.py

Option 1 should be preferred if this change is permanent.
Option 2 should be preferred if this change is temporary.
In option 2 the original python wrapper scripts remain unmodified.


The file seems to be called `Dist_Events_Real.csv`and the relevant argument 
seems to be `Demand_outpath` defined in this line of `cbm_run.py`:
```
Demand_outpath=os.path.join(cbmProjectDir_results2, "Dist_Events_Real.csv")
```

Then there is a line that says:
```
# ingest harvest demand CSV table into project db 
sitDb.ExecuteQuery("""CREATE TABLE HARVEST_DEMAND ( _1 VARCHAR, _2 VARCHAR, _3 VARCHAR, _4 VARCHAR, _5 VARCHAR, _6 VARCHAR, _7 VARCHAR, UsingID VARCHAR, SWStart INTEGER, SWEnd INTEGER, HWStart INTEGER, HWEnd INTEGER, Min_since_last_Dist INTEGER, Max_since_last_Dist INTEGER, Last_Dist_ID VARCHAR, Min_tot_biom_C FLOAT, Max_tot_biom_C FLOAT, Min_merch_soft_biom_C FLOAT, Max_merch_soft_biom_C FLOAT, Min_merch_hard_biom_C FLOAT, Max_merch_hard_biom_C FLOAT, Min_tot_stem_snag_C FLOAT, Max_tot_stem_snag_C FLOAT, Min_tot_soft_stem_snag_C FLOAT, Max_tot_soft_stem_snag_C FLOAT, Min_tot_hard_stem_snag_C FLOAT, Max_tot_hard_stem_snag_C FLOAT, Min_tot_merch_stem_snag_C FLOAT, Max_tot_merch_stem_snag_C FLOAT, Min_tot_merch_soft_stem_snag_C FLOAT, Max_tot_merch_soft_stem_snag_C FLOAT, Min_tot_merch_hard_stem_snag_C FLOAT, Max_tot_merch_hard_stem_snag_C FLOAT, Efficency FLOAT, Sort_Type VARCHAR, Measurement_type VARCHAR, Amount FLOAT, Dist_Type_ID VARCHAR, Step INTEGER );""")
```

And then the disturbance table called `Demand_outpath` is inserted into the database with 
the following call:
```
        # insert a row to the new table for each row of data in the CSV file
        with open(Demand_outpath, "rb") as f: # open the CSV file 
            reader = csv.reader(f, delimiter=",") #creates a CSV reader to parse the opened file
            skip = True
        
            for i, line in enumerate(reader):
                if skip: #skip header
                    skip=False
                    continue
                
                insertQuery = """Insert into HARVEST_DEMAND 
                    (_1,_2,_3,_4,_5,_6,_7,UsingID,SWStart,SWEnd,HWStart,HWEnd,Min_since_last_Dist,Max_since_last_Dist,Last_Dist_ID,Min_tot_biom_C,Max_tot_biom_C,Min_merch_soft_biom_C,Max_merch_soft_biom_C,Min_merch_hard_biom_C,Max_merch_hard_biom_C,Min_tot_stem_snag_C,Max_tot_stem_snag_C,Min_tot_soft_stem_snag_C,Max_tot_soft_stem_snag_C,Min_tot_hard_stem_snag_C,Max_tot_hard_stem_snag_C,Min_tot_merch_stem_snag_C,Max_tot_merch_stem_snag_C,Min_tot_merch_soft_stem_snag_C,Max_tot_merch_soft_stem_snag_C,Min_tot_merch_hard_stem_snag_C,Max_tot_merch_hard_stem_snag_C,Efficency,Sort_Type,Measurement_type,Amount,Dist_Type_ID,Step)
                    VALUES
                    (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)""" # build a parameterized query with column names from harvest_demand.csv
                
                sitDb.ExecuteQuery(insertQuery, params=line) 
                # the params argument will substitute each "?" that appears in insertQuery with the ordered value in the line
```


# Double the number of disturbance events inside the database
Roberto: 
> Within a disturbance matrix, each disturbance event may refer only to a unique combination of classifiers, which cannot change from one time step to the following one. For example, if Dist event 12, in time step 1, is defined using the combination:

    _1=?
    _2=PA
    _3=?
    _4=?

> ... and in time step 2, the same dist event is defined using a different combination of classifiers:

    _1=?
    _2=PA
    _3=TEST
    _4=H

> this disturbance will be ignored in time step 2!

This can be achieved in two steps: 
 1. add 1000 to the ids of  disturbance events table 
 2. duplicate the disttypes table add 1000 to the ids of the duplicated part.


## Developing a function that uses metaprogramming to modify an unspecified variable name
See Hadley Wickham's book on programming with R.


```r
# Could I use a formula to be more generic?
all.vars(y ~ y * 1000)

add1000 <- function(dtf, var){
  varright <- enquo(var)
  varleft <- quo_name(enquo(var))
  dtf %>% 
    mutate(!!varleft := 1000 + (!!varright))
}
add1000(iris, Sepal.Length)
```


## Use the plumbing function duplicateandshift()
See also example in the function help page ?duplicateandshift

```r
disttypesCZ2 <- duplicateandshift(disttypesCZ, DisturbanceTypeID, 1000) 
```
    

## Prepare a porcelaine function for use in the scripts

At which place should the duplication happen?
Some disturbance ids are originaly character variable.
There is a place in the script where they are changed to numeric variables. 
The duplication should happen after that place. 

Find out which scripts use `DisturbanceTypes.csv`:
```
$ grep -n DisturbanceTypes.csv *.py
cbm_update.py:107:    DistTypesPath=os.path.join(project_path, "DisturbanceTypes.csv")
upd_aws.py:43:        DistTypesPath=os.path.join(project_path, "DisturbanceTypes.csv")

$ grep -n DisturbanceTypes.csv *.R
expandFaws.R:32:      distTypesPath <- paste0(project_path, "/DisturbanceTypes.csv")
```

The change to numeric ids happens in line 36 of `expandFaws.R`:
```
# assign numeric values to disturbance Types
dt<-read.csv (distTypesPath,stringsAsFactors = F)
dt$numID<-rank(dt$DisturbanceTypeID)
```
Then these disturbance ids can be duplicated directly at the end of expandFaws.R.

But duplicated disturbances (id+1000) should only be mapped to disturbances used after the historical period.


## What disturbance tables are used by the CBM wrapper scripts?
`expandFaws.R` is only used by `upd_aws.py`.
```
$ grep -n expandFaws.R *.py
upd_aws.py:78:        R_AWS_Path = os.path.join(base_path, "expandFaws.R")
$ grep -n R_AWS_Path *.py
upd_aws.py:78:        R_AWS_Path = os.path.join(base_path, "expandFaws.R")
upd_aws.py:79:        self.callRScript(R_Executable_Path, R_AWS_Path, R_script_args=[project_path])
```

The `expandFaws.R` script uses five tables related to disturbance events: 
```
$ grep -n ist expandFaws.R
[...]
26:HistDistEventsPath <- paste0(project_path, "/Hist_Dist_Events.csv")
27:distEventsPath <- paste0(project_path, "/Dist_Events_Const.csv")
28:distEventsCalibrationPath <- paste0(project_path, "/Dist_Events_Const_Cal.csv")
31:natDistPath <- paste0(project_path, "/natDist.csv")
32:distTypesPath <- paste0(project_path, "/DisturbanceTypes.csv")
[...]
```

Where are the historical disturbances ?
```
$ grep -n HistDistEventsPath expandFaws.R
26:HistDistEventsPath <- paste0(project_path, "/Hist_Dist_Events.csv")
196:disthist<- readLines(HistDistEventsPath)
211:writeLines(c(distHeader,tab[[3]][-1]), con = HistDistEventsPath)
241:for (j in c(distEventsPath,distEventsCalibrationPath, HistDistEventsPath)){
```

Where are disturbances after the historical period created? 



### What disturbances are used in `cbm_update.py`?


```
$ grep -ni hist_dist_events cbm_update.py
109:        HistDistEventsPath=os.path.join(project_path, "Hist_Dist_Events.csv")
200:        sitDb.ExecuteQuery("""CREATE TABLE HIST_DIST_EVENTS_CBM (
316:                insertQuery = """Insert into HIST_DIST_EVENTS_CBM
423:             "HIST_DIST_EVENTS_CBM",
```

`cbm_update.py` runs CBM with two different disturbance tables :
 * once with `HIST_DIST_EVENTS_CBM` and 
 * once with `Dist_Events_Const_Cal`

```
$ grep -ni -A 9 self.runPysit cbm_update.py
418:        self.runPysit(pysitpath,
419-             pysitScriptPath,
420-             archiveIndexPathWorking,
421-             sitPathCopied,
422-             workingProjectPath,
423-             "HIST_DIST_EVENTS_CBM",
424-             "HIST_TRANSITION_CBM",
425-             "YTS_HISTORICAL_SORTED",
426-             runLength)
427-
--
581:        self.runPysit(pysitpath,
582-             pysitScriptPath,
583-             archiveIndexPathWorking,
584-             sitPathCopied,
585-             workingProjectPath,
586-             "Dist_Events_Const_Cal",
587-             "TRANSITION_CBM",
588-             "YTS_CURRENT_SORTED",
589-             runLength)
590-
```

#### `HIST_DIST_EVENTS_CBM`

`HIST_DIST_EVENTS_CBM` is created from `Hist_Dist_Events.csv`.

Details:
In a SQL insert Query of the form:
```
with open(HistDistEventsPath, "rb") as f: # open the CSV file 
[...]
insertQuery = """Insert into HIST_DIST_EVENTS_CBM
```
The file`HistDistEventsPath` corresponding to 
```
HistDistEventsPath=os.path.join(project_path, "Hist_Dist_Events.csv")
```
Is inserted into the database table `HIST_DIST_EVENTS_CBM`.


#### `Dist_Events_Const_Cal`

`Dist_Events_Const_Cal` is created from `Dist_Events_Const_Cal.csv`

Details:
In a SQL insert Query of the form:
```
with open(DistEventsCalibPath, "rb") as f: # open the CSV file 
[...]
insertQuery = """Insert into Dist_Events_Const_Cal
```
The file `DistEventsCalibPath` coresponding to
```
DistEventsCalibPath=os.path.join(project_path, "Dist_Events_Const_Cal.csv")
```
Is inserted into the database table `Dist_Events_Const_Cal`


### What disturbances are used in `cbm_mws.py`?

`cbm_mws.py` runs CBM with two different disturbance tables 
called `DIST_EVENTS_I` and `DIST_EVENTS_II`:

```
$ grep -ni -B 4 -A 9 self.runPysit cbm_mws.py
445-       
446-
447-
448-        # call the pysit script for the no management run
449:        self.runPysit(pysitpath,
450-                 pysitScriptPath,
451-                 archiveIndexPathWorking,
452-                 sitPathCopied,
453-                 workingProjectPath,
454-                 "DIST_EVENTS_I",
455-                 "TRANSITION_CBM",
456-                 "YTS_CURRENT_SORTED",
457-                 runLengthNew)
458-
--
552-        # insert result from R script to project db and generate new disturbance request table
553-        self.mwsHarvestDemand(sitDb, R_outpath)
554-
555-        # call the pysit script for the MWS run
556:        self.runPysit(pysitpath,
557-                 pysitScriptPath,
558-                 archiveIndexPathWorking,
559-                 sitPathCopied,
560-                 workingProjectPath,
561-                 "DIST_EVENTS_II",
562-                 "TRANSITION_CBM",
563-                 "YTS_CURRENT_SORTED",
564-                 runLengthNew)
565-
--
596-        # if necessary, loop to adjust harvest request
597-        while os.path.exists(checkPath):
598-
599-            self.mwsHarvestDemand(sitDb, R_outpath)
600:            self.runPysit(pysitpath,
601-                     pysitScriptPath,
602-                     archiveIndexPathWorking,
603-                     sitPathCopied,
604-                     workingProjectPath,
605-                     "DIST_EVENTS_II",
606-                     "TRANSITION_CBM",
607-                     "YTS_CURRENT_SORTED",
608-                     runLengthNew)
609-
```

The disturbance tables `DIST_EVENTS_I` and `DIST_EVENTS_II`
appear in the following places inside `cbm_mws.py`:
```
$ grep  -ni dist_events_i cbm_mws.py
195:        tableNames = ["MAX_HARVEST_REQUEST_I", "DIST_EVENTS_II", "Dist_Types"]
243:            INTO DIST_EVENTS_II
312:        sitDb.ExecuteQuery("""SELECT DIST_EVENTS_II.[_1], DIST_EVENTS_II.[_2], DIST_EVENTS_II.Dist_Type_ID, DIST_EVENTS_II.TimeStep, Su
m(DIST_EVENTS_II.Amount) AS TOT_Amount_Exp
314:            FROM DIST_EVENTS_II
315:            GROUP BY DIST_EVENTS_II.[_1], DIST_EVENTS_II.[_2], DIST_EVENTS_II.Dist_Type_ID, DIST_EVENTS_II.TimeStep;""")
416:        tableNames = ["DIST_EVENTS_I", "Time_Step_"]
431:        # prepare DIST_EVENTS_I with one "dummy" disturbance at the end of the timestep
434:            INTO DIST_EVENTS_I
439:        distQuery=("""Insert into DIST_EVENTS_I
454:                 "DIST_EVENTS_I",
561:                 "DIST_EVENTS_II",
605:                     "DIST_EVENTS_II",
```



#### `DIST_EVENTS_I`

`DIST_EVENTS_I` is generated in the main function `run`
```
def run(self, data_path, project_name, startYear, endYear):
# main function
```
Line 434 `DIST_EVENTS_I` is generated from `Dist_Events_Const`
by a SQL query of the form:
```
SELECT ...
INTO DIST_EVENTS_I
FROM Dist_Events_Const
WHERE (((Dist_Events_Const.Step)<0));
```
Then some content is injected inside it by a SQL query of the form:
```
distQuery=("""Insert into DIST_EVENTS_I 
(_1, _2, _3, _4, _5, _6, _7, UsingID, SWStart, SWEnd, HWStart, HWEnd, Min_since_last_Dist, Max_since_last_Dist, Last_Dist_ID, Min_tot_biom_C, Max_tot_biom_C, Min_merch_soft_biom_C, Max_merch_soft_biom_C, Min_merch_hard_biom_C, Max_merch_hard_biom_C, Min_tot_stem_snag_C, Max_tot_stem_snag_C, Min_tot_soft_stem_snag_C, Max_tot_soft_stem_snag_C, Min_tot_hard_stem_snag_C, Max_tot_hard_stem_snag_C, Min_tot_merch_stem_snag_C, Max_tot_merch_stem_snag_C, Min_tot_merch_soft_stem_snag_C, Max_tot_merch_soft_stem_snag_C, Min_tot_merch_hard_stem_snag_C, Max_tot_merch_hard_stem_snag_C, Efficency, Sort_Type, Measurement_type, Amount, Dist_Type_ID, Step)
VALUES
(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);""")
distline=('?','?','?','?','?','?','?',"FALSE",1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,3,"M",10,str(HistDist),runLengthNew)
sitDb.ExecuteQuery(distQuery, params=distline) 
```
This seems to insert default values.
The only variables are `HistDist` and `runLengthNew`. 
Concerning `HistDist`, str() is a python function that changes a variable to a string.
The code below is used to generate `HistDist` from the csv file `refYearPath` given by 
```
refYearPath=os.path.join(data_path, "refyears.csv")
```
```
#HistDist=str(0)
csvfile = open(refYearPath, 'rb') 
global x
global y
global z
refYears = csv.reader(csvfile, delimiter=',')
for pair in refYears:
    try:
        x,y,z = pair[0],pair[1],pair[3]
        if x == project_name:
            HistDist = z
    except IndexError:
        print "A line in the file doesn't have enough entries."
```
`runLengthNew` is defined line 409
```
# calculate simulation length
runLengthNew=int(endYear)-int(startYear)
```


#### `DIST_EVENTS_II`
`DIST_EVENTS_II` is generated inside the function `mwsHarvestDemand`
```
def mwsHarvestDemand(self, sitDb, R_outpath):
# function that ingests allocated harvest demand CSV from R script into project db
```
Line 243 `DIST_EVENTS_II` is generated from `MAX_HARVEST_REQUEST_I` and `Dist_Types` 
by a SQL query of the form:
```
SELECT ... into DIST_EVENTS_II
FROM MAX_HARVEST_REQUEST_I LEFT JOIN Dist_Types
```
For information line 312 `DIST_EVENTS_II` is used to generate `Tot_Amoun_Exp`


### What disturbances are used in `cbm_run.py`?

`cbm_run.py` run CBM with only one disturbance table called `HARVEST_DEMAND`.

```
$ grep -ni -A 9 self.runPysit cbm_run.py
187:        self.runPysit(pysitpath,
188-                 pysitScriptPath,
189-                 archiveIndexPathWorking,
190-                 sitPathCopied,
191-                 workingProjectPath,
192-                 "HARVEST_DEMAND",
193-                 "TRANSITION_CBM",
194-                 "YTS_CURRENT_SORTED",
195-                 runLengthNew)
196-
```


#### `HARVEST_DEMAND`
`HARVEST_DEMAND` is created from `Dist_Events_Real.csv`

Details:
With a query of the form
```
with open(Demand_outpath, "rb") as f: # open the CSV file 
insertQuery = """Insert into HARVEST_DEMAND 
```
The file `Demand_outpath` corresponding to `Dist_Events_Real.csv`
```
Demand_outpath=os.path.join(cbmProjectDir_results2, "Dist_Events_Real.csv")
```
is inserted into the database table `HARVEST_DEMAND`

This is the only file that should use the duplicated disturbance Types with id number > 1000. 
A quick fix would be to use +1000 in the code that generates `Dist_Events_Real.csv`.
This file is coming from FUSION. 
Maybe the id should be updated before sending the data to fusion. 
The file sent to fusion is 



